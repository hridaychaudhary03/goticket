<?php

namespace App\Providers;

use App\Models\siteGeneralSettings;
use App\Models\timezone;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;
use App\Repositories\Interfaces\EventRepositoryInterfaces;
use App\Repositories\Interfaces\CategoryRepositoryInterfaces;
use App\Repositories\Interfaces\TicketRepositoryInterfaces;
use App\Repositories\Interfaces\advertiesmentRepositoryInterfaces;
use App\Repositories\EventRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\TicketRepository;
use App\Repositories\advertiesmentRepository;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(EventRepositoryInterfaces::class, EventRepository::class);
        $this->app->bind(CategoryRepositoryInterfaces::class, CategoryRepository::class);
        $this->app->bind(TicketRepositoryInterfaces::class, TicketRepository::class);
        $this->app->bind(advertiesmentRepositoryInterfaces::class, advertiesmentRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::share('siteGeneralSettings', siteGeneralSettings::find(2));
        $timezone = timezone::find(1);
        $newtimezone = $timezone->timezone;
        date_default_timezone_set($newtimezone);
        Paginator::useBootstrap();
    }
}
