<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class siteGeneralSettings extends Model
{
    use HasFactory;
    protected $table = 'generalsettings';
    protected $primaryKey = 'id';
}
