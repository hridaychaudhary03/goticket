<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ticket_category extends Model
{
    use HasFactory;
    protected $fillable = [
        'ticket_category',
        'event_id',
        'category_description',
        'minimum_ticket',
        'price',
        'fee',
        'ticket_category_event_date',
        'promo_code',
        'max',
        'sold',
        'reservation',
        'ticket_info',
        'type',
        'active',
        'sold_out_email',
        'can_overlap',
        'can_steal',
    ];
    protected $table = 'ticket_category';
    protected $primaryKey = 'id';
    function eventTicket(){
        return $this->hasMany('App\Models\event_tickets', "ticket_category_id", "id");
    }
}
