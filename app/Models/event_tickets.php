<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class event_tickets extends Model
{
    use HasFactory;
    protected $fillable = [
        'event_id',
        'ticket_category_id',
        'max',
        'sold',
    ];
    protected $table = 'event_tickets';
    protected $primaryKey = 'id';
}
