<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;
use App\Models\User;
use App\Models\siteGeneralSettings;
use App\Models\timezone;
use App\Models\Events;
use Carbon\Carbon;

class AdminController extends Controller
{
    // get admin information
    public function getAdministrator(){
        $users = User::whereRoleIs('admin')->orWhereRoleIs('Band')->get();;
        return view('admin.administrator')->with('users', $users);
    }
     // get admin timezone
    public function getTimezone(){
        $timezone = timezone::find(1);
        $data = compact('timezone');
        return view('admin.timezone')->with($data);
    }
      // get admin timezone
      public function updateTimezone(Request $request){
        $timezone = timezone::find(1);
        $timezone -> timezone = $request['timezone'];
        $timezone -> user_id = $request['user_id'];
        $timezone -> save();
        return redirect('admin/time-zone')->with('message', 'TimeZone Successfully Updated');
    }
    public function getGeneralStings(){
        $General = siteGeneralSettings::find(2);
        $data = compact('General');
        return view('admin.generalSettings')->with($data);
    }
    public function updateGeneralStings(Request $request ){
    //    echo "<pre>";
    //    print_r($request->all());
       $General = siteGeneralSettings::find(2);
       $General ->site_name = $request['site_name'];
       $General ->contact_email = $request['contact_email'];
       $General ->user_id = $request['user_id'];
       $General ->contact_phone = $request['contact_phone'];
       $General ->contact_address = $request['contact_address'];
       $General ->facebook_page_url = $request['facebook_page_url'];
       $General ->twitter_page_url = $request['twitter_page_url'];
       $General ->youtube_page_url = $request['youtube_page_url'];
       $General ->default_page_title = $request['default_page_title'];
       $General ->default_lang = $request['default_lang'];
       $General ->default_meta_keys = $request['default_meta_keys'];
       $General ->default_meta_desc = $request['default_meta_desc'];
       $General ->site_offline_msg = $request['site_offline_msg'];
       $General ->site_status = $request['site_status'];
       $General ->initial_event_display_count = $request['initial_event_display_count'];
       $General ->remember_token = $request['_token'];
       $General ->save();
       return redirect('admin/settings/general')->with('message', 'Site General Settings Successfully Updated');
    }
    public function addEvents()
    {
        return view('admin.addEvent');
    }
}
