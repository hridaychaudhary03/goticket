<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Http\Request;

class advertiesmentController extends Controller
{
    use HasFactory;
    public function getAdvertiesment($id)
    {
        $data = compact("id");
        return view("admin.eventAds")->with($data);
    }
    public function createOrUpdateAdvertiesment(Request $request)
    {
        // dd($request->all());
        $files = $request->aidsImage;
        // dd($files);
        foreach ($files as $req) {
            // dd($req);
            $fileName = time() . "advertiesment." . $req->file('image')->getClientOriginalExtension();
            $req->file('image')->storeAs('public/advertiesment', $fileName);
        }
    }
}
