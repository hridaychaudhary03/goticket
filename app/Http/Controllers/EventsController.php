<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Events;
use Illuminate\Support\Str;
use App\Repositories\Interfaces\EventRepositoryInterfaces;

class EventsController extends Controller
{
    use HasFactory;
    private $EventRepository;
    public function __construct(EventRepositoryInterfaces $EventRepository)
    {
        $this->EventRepository = $EventRepository;
    }
    public function eventLists()
    {
        $status = 'notcancelled';
        $Events = $this->EventRepository->all($status);
        $data = compact('Events');
        return view('admin.eventList')->with($data);
    }
    public function cancelEventList()
    {
        $status = 'cancelled';
        $Events = $this->EventRepository->all($status);
        $data = compact('Events');
        return view('admin.cancelEvent')->with($data);
    }
    public function storeEvent(Request $request)
    {
        $start_dateNew = $request['start_date'] . ' ' . $request['in_time'];
        $end_dateNew = $request['end_date'] . ' ' . $request['out_time'];
        $publish_dateNew = $request['event_publish_date'] . ' ' . $request['event_publish_time'];
        $userId =  Auth::user()->id;
        $this->validate($request, [
            'title' => 'required',
            'start_date' => 'required|date_format:Y-m-d',
            'in_time' => 'required',
            'end_date' => 'required|date_format:Y-m-d',
            'out_time' => 'required',
            'max_participants' => 'required',
            'event_publish_date' => 'required|date_format:Y-m-d',
            'event_publish_time' => 'required',
            'street_address' => 'required',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);
        $fileName = time() . "event." . $request->file('image')->getClientOriginalExtension();
        $request->file('image')->storeAs('public/upload', $fileName);
        $event = new Events;
        $event->title = $request['title'];
        $event->slug = Str::slug($request->title);
        $event->description = $request['description'];
        $event->user_id =  $userId;
        $event->start_date = $start_dateNew;
        $event->end_date = $end_dateNew;
        $event->max_participants = $request['max_participants'];
        $event->event_publish_date = $publish_dateNew;
        $event->street_address = $request['street_address'];
        $event->city = $request['city'];
        $event->event_form_location = $request['event_form_location'];
        $event->event_form_featured = $request['event_form_featured'];
        $event->split_price = $request['split_price'];
        $event->payment_type = $request['payment_type'];
        $event->is_ehf_invoice = $request['is_ehf_invoice'];
        $event->event_form_vat = $request['event_form_vat'];
        $event->event_form_send_ticket = $request['event_form_send_ticket'];
        $event->invoice_fee = $request['invoice_fee'];
        $event->due_date = $request['due_date'];
        $event->invoice_due_date_text_flag = $request['invoice_due_date_text_flag'];
        $event->add_instruction = $request['add_instruction'];
        $event->invoice_account_no = $request['invoice_account_no'];
        $event->no_registration = $request['no_registration'];
        $event->image = $fileName;
        $event->save();
        $postId = $event->id;
        // $data = compact("postId");
        // return view('admin.addTicketCategories/'.$data)->with('message', 'Event Created Succesfully');
        return redirect('admin/event/ticket-category/'.$postId)->with('message', 'Event Created Succesfully');
    }
    public function trashEvent($id)
    {
        $event = $this->EventRepository->find($id);
        if (!is_null($event)) {
            $event->delete();
        }
        return redirect('admin/event/event-list')->with('message', 'Event Moved to trash Succesfully');
    }
    public function deleteEvent($id)
    {
        $event = Events::onlyTrashed()->find($id);
        if (!is_null($event)) {
            $event->forceDelete();
        }
        return redirect('admin/event/event-trashed')->with('message', 'Event permanently deleted');
    }
    public function restoreEvent($id)
    {
        $event = Events::onlyTrashed()->find($id);
        if (!is_null($event)) {
            $event->restore();
        } else {
            return redirect('admin/event/event-trashed')->with('message', 'Event Not Found');
        }
        return redirect('admin/event/event-list')->with('message', 'Event Restored Succesfully');
    }
    public function trashedEvent()
    {
        $Events = Events::onlyTrashed()->get();
        $data = compact('Events');
        return view('admin.eventTrashed')->with($data);
    }
    public function editEvent($id)
    {
        $event = Events::find($id);
        if (is_null($event)) {
            return redirect('admin/event/event-list')->with('error', 'Event Not found!');
        }
        $data = compact('event');
        return view('admin.editEvent')->with($data);
    }
    public function updateEvent($id, Request $request)
    {

        $start_dateNew = $request['start_date'] . ' ' . $request['in_time'];
        $end_dateNew = $request['end_date'] . ' ' . $request['out_time'];
        $publish_dateNew = $request['event_publish_date'] . ' ' . $request['event_publish_time'];
        $userId =  Auth::user()->id;
        $this->validate($request, [
            'title' => 'required',
            'start_date' => 'required',
            'in_time' => 'required',
            'end_date' => 'required',
            'out_time' => 'required',
            'max_participants' => 'required',
            'event_publish_date' => 'required',
            'event_publish_time' => 'required',
            'street_address' => 'required',
        ]);
        if (!is_null($request->image)) {
            $fileName = time() . "event." . $request->file('image')->getClientOriginalExtension();
            $request->file('image')->storeAs('public/upload', $fileName);
        } else {
            $fileName = $request->imageedit;
        }
        $event = Events::find($id);
        $event->title = $request['title'];
        $event->slug = Str::slug($request->title);
        $event->description = $request['description'];
        $event->user_id =  $userId;
        $event->start_date = $start_dateNew;
        $event->end_date = $end_dateNew;
        $event->max_participants = $request['max_participants'];
        $event->event_publish_date = $publish_dateNew;
        $event->street_address = $request['street_address'];
        $event->city = $request['city'];
        $event->event_form_location = $request['event_form_location'];
        $event->event_form_featured = $request['event_form_featured'];
        $event->split_price = $request['split_price'];
        $event->payment_type = $request['payment_type'];
        $event->is_ehf_invoice = $request['is_ehf_invoice'];
        $event->event_form_vat = $request['event_form_vat'];
        $event->event_form_send_ticket = $request['event_form_send_ticket'];
        $event->invoice_fee = $request['invoice_fee'];
        $event->due_date = $request['due_date'];
        $event->invoice_due_date_text_flag = $request['invoice_due_date_text_flag'];
        $event->add_instruction = $request['add_instruction'];
        $event->invoice_account_no = $request['invoice_account_no'];
        $event->no_registration = $request['no_registration'];
        $event->image = $fileName;
        $event->save();
        $postId = $id;
        // $data = compact("postId");
        return redirect('admin/event/ticket-category/'.$postId)->with('message', 'Event Edited Succesfully');
    }
    public function cancelEvent($id)
    {
        $Events = $this->EventRepository->find($id);
        if (!$Events) {
            return redirect('admin/event/event-list')->with('error', 'Event not found');
        }
        $Events->requested_status = 'cancelled';
        $Events->save();
        return redirect('admin/event/event-list')->with('success', 'Event cancelled successfully');
    }
    public function processingEvent($id)
    {
        $Events = $this->EventRepository->find($id);
        if (!$Events) {
            return redirect('admin/event/event-list')->with('error', 'Event not found');
        }
        $Events->requested_status = 'processing';
        $Events->save();
        return redirect('admin/event/event-list')->with('success', 'Event Processing successfully');
    }
}
