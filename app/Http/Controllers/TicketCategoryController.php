<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Http\Request;
use App\Repositories\Interfaces\EventRepositoryInterfaces;
use App\Repositories\Interfaces\CategoryRepositoryInterfaces;
use App\Models\ticket_category;

class TicketCategoryController extends Controller
{
    use HasFactory;
    private $EventRepository;
    private $CategoryRepository;
    public function __construct(EventRepositoryInterfaces $EventRepository, CategoryRepositoryInterfaces $CategoryRepository)
    {
        $this->EventRepository = $EventRepository;
        $this->CategoryRepository = $CategoryRepository;
    }
    public function addTicketCategory($id, Request $request)
    {
        // return ticket_category::with("eventTicket")->where('event_id', $id)->get();
        $event = $this->EventRepository->find($id);
        $categories = ticket_category::with("eventTicket")->where('event_id', $id)->get();
        $data = compact('event', 'categories');
        return view('admin.addTicketCategories')->with($data);
    }

    public function updateCreateTicketCategory(Request $request)
    {
        $category_date = $request['ticket_category_event_date'] . ' ' . $request['ticket_category_event_time'];
        $category = ticket_category::updateOrCreate(
            ['id' => $request->id],
            [
                'ticket_category' => $request->ticket_category,
                'event_id' => $request->event_id,
                'category_description' => $request->category_description,
                'minimum_ticket' => $request->minimum_ticket,
                'price' => $request->price,
                'fee' => $request->fee,
                'ticket_category_event_date' => $category_date,
                'promo_code' => $request->promo_code,
                'max' => $request->max,
                'sold' => $request->sold,
                'reservation' => $request->reservation,
                'ticket_info' => $request->ticket_info,
                'type' => $request->ticket_category_type,
                'active' => $request->active,
                'sold_out_email' => $request->sold_out_email,
                'can_overlap' => $request->can_overlap,
                'can_steal' => $request->can_steal,
            ]
        );
        $data = $category->id;
        return response()->json($data);
    }

    public function deleteTicketCategory($id)
    {
        $category = ticket_category::find($id);
        $category->delete();
        return response()->json("category successfully deleted");
    }
}
