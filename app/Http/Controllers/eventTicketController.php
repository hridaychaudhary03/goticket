<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Repositories\Interfaces\TicketRepositoryInterfaces;
use Illuminate\Http\Request;
use App\Models\event_tickets;


class eventTicketController extends Controller
{
    use HasFactory;
    private $TicketRepository;
    public function __construct(TicketRepositoryInterfaces $TicketRepository){
        $this->TicketRepository = $TicketRepository;
    }
    public function updateCreateEventTicket(Request $request)
    {
        $Ticket = event_tickets::updateOrCreate(
            ['id' => $request->id],
            [
                'ticket_category_id' => $request->categoryid,
                'event_id' => $request->eventid,
                'max' => $request->max,
            ]
        );
        $data = $Ticket->id;
        return response()->json($data);
    }

    public function deleteEventTicket($id)
    {
        $Ticket = event_tickets::find($id);
        $Ticket -> delete();
        return response()->json("category successfully deleted");
    }
}
