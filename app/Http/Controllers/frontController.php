<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Events;

class frontController extends Controller
{
    public function frontPage()
    {
        $Events = Events::where('requested_status', '!=', 'cancelled')->get();
        $data = compact('Events');
        return view('front.pages.front')->with($data);
    }
}
