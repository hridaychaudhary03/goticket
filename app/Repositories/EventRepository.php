<?php

namespace App\Repositories;

use App\Repositories\Interfaces\EventRepositoryInterfaces;
use App\Models\Events;
use App\Models\siteGeneralSettings;

class EventRepository implements EventRepositoryInterfaces
{
    public function all($status)
    {
        $sitesettings=siteGeneralSettings::find(2);
       $displayCount = $sitesettings->initial_event_display_count;
       if($status == 'cancelled'){
        return  Events::orderBy('created_at', 'desc')->where('requested_status', '=', 'cancelled')->paginate($displayCount);
       }else{
        return Events::orderBy('created_at', 'desc')->where('requested_status', '!=', 'cancelled')->paginate($displayCount);
       }
    }
    public function find($id)
    {
        return Events::find($id);
    }
    public function store($data)
    {
        Events::create($data);
    }
}
