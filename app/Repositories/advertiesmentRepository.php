<?php

namespace App\Repositories;

use App\Repositories\Interfaces\advertiesmentRepositoryInterfaces;
use App\Models\ticket_category;
use App\Models\siteGeneralSettings;

class advertiesmentRepository implements advertiesmentRepositoryInterfaces
{
    public function all($status)
    {
        $sitesettings = siteGeneralSettings::find(2);
        $displayCount = $sitesettings->initial_event_display_count;
        if ($status == 'cancelled') {
            return  ticket_category::orderBy('created_at', 'desc')->where('requested_status', '=', 'cancelled')->paginate($displayCount);
        } else {
            return ticket_category::orderBy('created_at', 'desc')->where('requested_status', '!=', 'cancelled')->paginate($displayCount);
        }
    }
    public function find($id)
    {
        return ticket_category::find($id);
    }
    public function store($data)
    {
        ticket_category::create($data);
    }
}
