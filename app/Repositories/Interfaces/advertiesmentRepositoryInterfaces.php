<?php

namespace App\Repositories\Interfaces;

interface advertiesmentRepositoryInterfaces
{
    function all($status);
    function find($id);
    function store($data);
}
