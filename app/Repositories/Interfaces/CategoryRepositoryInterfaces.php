<?php

namespace App\Repositories\Interfaces;

interface CategoryRepositoryInterfaces
{
    function all($status);
    function find($id);
    function store($data);
}
