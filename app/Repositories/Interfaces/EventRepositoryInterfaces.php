<?php

namespace App\Repositories\Interfaces;

interface EventRepositoryInterfaces
{
    function all($status);
    function find($id);
    function store($data);
}
