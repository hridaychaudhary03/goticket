<?php

namespace App\Repositories\Interfaces;

interface TicketRepositoryInterfaces
{
    function all($status);
    function find($id);
    function store($data);
}
