<?php

namespace App\Repositories;

use App\Repositories\Interfaces\TicketRepositoryInterfaces;
use App\Models\event_tickets;
use App\Models\siteGeneralSettings;

class TicketRepository implements TicketRepositoryInterfaces
{
    public function all($status)
    {
        $sitesettings=siteGeneralSettings::find(2);
       $displayCount = $sitesettings->initial_event_display_count;
       if($status == 'cancelled'){
        return  event_tickets::orderBy('created_at', 'desc')->where('requested_status', '=', 'cancelled')->paginate($displayCount);
       }else{
        return event_tickets::orderBy('created_at', 'desc')->where('requested_status', '!=', 'cancelled')->paginate($displayCount);
       }
    }
    public function find($id)
    {
        return event_tickets::find($id);
    }
    public function store($data)
    {
        event_tickets::create($data);
    }
}
