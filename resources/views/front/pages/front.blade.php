@extends('front.main')
@section('main-section')
<section class="event-lists-section">
    <div class="container-xl">
        <div class="row">
            @if($Events->count()> 0)
            @foreach ( $Events as $Event)
            <div class="col-md-3">
                <div class="card" style="width: 18rem;">
                    <img src="{{asset('storage/upload/'.$Event->image)}}" class="card-img-top" alt="{{$Event->title}}">
                    <div class="card-body">
                        <h5 class="card-title">{{$Event->title}}</h5>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
            </div>
            @endforeach
            @else
            <div class="col">
                <p>Event Not Found</p>
            </div>
            @endif
        </div>
    </div>
</section>
@endsection
