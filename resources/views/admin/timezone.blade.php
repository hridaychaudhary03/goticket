@extends('admin.main')
@section('main-section')
<div class="inner-heading">
    <h1 class="page-header-title"><i class="icon-flag-alt"></i>Timezone</h1>
</div>
<div class="breadcrumb-wrapper">
    <ul class="breadcrumb-lists">
        <li class="bread-item"><a href="{{url('admin/dashboard')}}">Dashboard</a> <i class="icon-angle-right"></i></li>
        <li class="bread-item active"><a href="#">Timezone</a></li>
    </ul>
</div>
<div class="inner-wrapper">
    @if(session()->has('message'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{session()->get('message')}}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    <div class="card">
        @php
        echo date("Y/m/d h:i:sa");
        @endphp
        <div>
            <p>The current time zone is: {{ date_default_timezone_get(); }}</p>
        </div>
        <form action="{{url('/admin/time-zone')}}" method="POST">
            @csrf
            <input type="hidden" name="user_id" value="{{Auth::user()->id }}">
            <div class="mb-3">
                <select class="form-control form-select" aria-label="Default select example" name="timezone">
                    <option value="America/Chicago" {{$timezone->timezone == 'America/Chicago' ? "selected" :
                        ""}}>GMT-6:00
                    </option>
                    <option value="America/New_York" {{$timezone->timezone == 'America/New_York' ? "selected" :
                        ""}}>GMT-5:00
                    </option>
                    <option value="America/La_Paz" {{$timezone->timezone == 'America/La_Paz' ? "selected" :
                        ""}}>GMT-4:00
                    </option>
                    <option value="America/Sao_Paulo" {{$timezone->timezone == 'America/Sao_Paulo' ? "selected" :
                        ""}}>GMT-3:00
                    </option>
                    <option value="America/Noronha" {{$timezone->timezone == 'America/Noronha' ? "selected" :
                        ""}}>GMT-2:00
                    </option>
                    <option value="Atlantic/Azores" {{$timezone->timezone == 'Atlantic/Azores' ? "selected" :
                        ""}}>GMT-1:00
                    </option>
                    <option value="GMT" {{$timezone->timezone == 'GMT' ? "selected" : ""}}>GMT</option>
                    <option value="Europe/Amsterdam" {{$timezone->timezone == 'Europe/Amsterdam' ? "selected" :
                        ""}}>GMT+1:00
                    </option>
                    <option value="Europe/Athens" {{$timezone->timezone == 'Europe/Athens' ? "selected" : ""}}>GMT+2:00
                    </option>
                    <option value="Europe/Moscow" {{$timezone->timezone == 'Europe/Moscow' ? "selected" : ""}}>GMT+3:00
                    </option>
                    <option value="Asia/Dubai" {{$timezone->timezone == 'Asia/Dubai' ? "selected" : ""}}>GMT+4:00
                    </option>
                    <option value="Asia/Karachi" {{$timezone->timezone == 'Asia/Karachi' ? "selected" : ""}}>GMT+5:00
                    </option>
                    <option value="Asia/Kathmandu" {{$timezone->timezone == 'Asia/Kathmandu' ? "selected" :
                        ""}}>GMT+5:45
                    </option>
                    <option value="Asia/Kolkata" {{$timezone->timezone == 'Asia/Kolkata' ? "selected" : ""}}>GMT+5:30
                    </option>
                    <option value="Asia/Dhaka" {{$timezone->timezone == 'Asia/Dhaka' ? "selected" : ""}}>GMT+6:00
                    </option>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Save Time Zone</button>
        </form>
    </div>
</div>
@endsection
