@extends('admin.main')
@section('main-section')
<div class="inner-heading">
    <h1 class="page-header-title"><i class="icon-flag-alt"></i>Edit Event</h1>
    <div class="event-btn-group">
        <a href="{{route('admin.eventlists')}}" class="btn btn-primary">Active Events</a>
    </div>
</div>
<div class="breadcrumb-wrapper">
    <ul class="breadcrumb-lists">
        <li class="bread-item"><a href="{{url('admin/dashboard')}}">Dashboard</a> <i class="icon-angle-right"></i></li>
        <li class="bread-item active"><a href="#">Edit Event</a></li>
    </ul>
</div>
<div class="inner-wrapper">
    <form action="{{route('admin.updateEvent', ['id'=>$event->id])}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="input-wrapper">
                        <label for="eventTitle" class="form-label">Title</label>
                        <input type="text" class="form-control" name="title" id="eventTitle" placeholder="Title"
                            value="{{$event->title}}">
                        @error('title')
                        <div class="lara-error">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="input-wrapper">
                        <label for="description" class="form-label">Tekst til arrangementet</label>
                        <textarea class="form-control cktextarea" name="description" id="description" rows="3">
                        {{$event->description}}
                    </textarea>
                    </div>
                    <div class="input-wrapper">
                        <div class="row">
                            <div class="col">
                                <label for="start_date" class="form-label">startdato og klokkeslett</label>
                                <div class="input-group mb-6">
                                    <span class="input-group-text" id="basic-addon2"><i
                                            class="icon-calendar"></i></span>
                                    <input type="text" class="form-control" id="start_date" name="start_date"
                                        placeholder="Start Date" value="{{date("Y-m-d",
                                        strtotime($event->start_date))}}">
                                    <span class="input-group-text" id="basic-addon2"><i class="icon-time"></i></span>
                                    @php
                                    $in_time = Carbon\Carbon::parse($event->start_date)->format('H:i:s');
                                    @endphp
                                    <select class="form-select" name="in_time">
                                        <option value="00:00:00" {{$in_time=='00:00:00' ? "selected" : "" }}>00:00:00
                                        </option>
                                        <option value="00:30:00" {{$in_time=='00:30:00' ? "selected" : "" }}>00:30:00
                                        </option>
                                        <option value="01:00:00" {{$in_time=='01:00:00' ? "selected" : "" }}>01:00:00
                                        </option>
                                        <option value="01:30:00" {{$in_time=='01:30:00' ? "selected" : "" }}>01:30:00
                                        </option>
                                        <option value="02:00:00" {{$in_time=='02:00:00' ? "selected" : "" }}>02:00:00
                                        </option>
                                        <option value="02:30:00" {{$in_time=='02:30:00' ? "selected" : "" }}>02:30:00
                                        </option>
                                        <option value="03:00:00" {{$in_time=='03:00:00' ? "selected" : "" }}>03:00:00
                                        </option>
                                        <option value="03:30:00" {{$in_time=='03:30:00' ? "selected" : "" }}>03:30:00
                                        </option>
                                        <option value="04:00:00" {{$in_time=='04:00:00' ? "selected" : "" }}>04:00:00
                                        </option>
                                        <option value="04:30:00" {{$in_time=='04:30:00' ? "selected" : "" }}>04:30:00
                                        </option>
                                        <option value="05:00:00" {{$in_time=='05:00:00' ? "selected" : "" }}>05:00:00
                                        </option>
                                        <option value="05:30:00" {{$in_time=='05:30:00' ? "selected" : "" }}>05:30:00
                                        </option>
                                        <option value="06:00:00" {{$in_time=='06:00:00' ? "selected" : "" }}>06:00:00
                                        </option>
                                        <option value="06:30:00" {{$in_time=='06:30:00' ? "selected" : "" }}>06:30:00
                                        </option>
                                        <option value="07:00:00" {{$in_time=='07:00:00' ? "selected" : "" }}>07:00:00
                                        </option>
                                        <option value="07:30:00" {{$in_time=='07:30:00' ? "selected" : "" }}>07:30:00
                                        </option>
                                        <option value="08:00:00" {{$in_time=='08:00:00' ? "selected" : "" }}>08:00:00
                                        </option>
                                        <option value="08:30:00" {{$in_time=='08:30:00' ? "selected" : "" }}>08:30:00
                                        </option>
                                        <option value="09:00:00" {{$in_time=='09:00:00' ? "selected" : "" }}>09:00:00
                                        </option>
                                        <option value="09:30:00" {{$in_time=='09:30:00' ? "selected" : "" }}>09:30:00
                                        </option>
                                        <option value="10:00:00" {{$in_time=='10:00:00' ? "selected" : "" }}>10:00:00
                                        </option>
                                        <option value="10:30:00" {{$in_time=='10:30:00' ? "selected" : "" }}>10:30:00
                                        </option>
                                        <option value="11:00:00" {{$in_time=='11:00:00' ? "selected" : "" }}>11:00:00
                                        </option>
                                        <option value="11:30:00" {{$in_time=='11:30:00' ? "selected" : "" }}>11:30:00
                                        </option>
                                        <option value="12:00:00" {{$in_time=='12:00:00' ? "selected" : "" }}>12:00:00
                                        </option>
                                        <option value="12:30:00" {{$in_time=='12:30:00' ? "selected" : "" }}>12:30:00
                                        </option>
                                        <option value="13:00:00" {{$in_time=='13:00:00' ? "selected" : "" }}>13:00:00
                                        </option>
                                        <option value="13:30:00" {{$in_time=='13:30:00' ? "selected" : "" }}>13:30:00
                                        </option>
                                        <option value="14:00:00" {{$in_time=='14:00:00' ? "selected" : "" }}>14:00:00
                                        </option>
                                        <option value="14:30:00" {{$in_time=='14:30:00' ? "selected" : "" }}>14:30:00
                                        </option>
                                        <option value="15:00:00" {{$in_time=='15:00:00' ? "selected" : "" }}>15:00:00
                                        </option>
                                        <option value="15:30:00" {{$in_time=='15:30:00' ? "selected" : "" }}>15:30:00
                                        </option>
                                        <option value="16:00:00" {{$in_time=='16:00:00' ? "selected" : "" }}>16:00:00
                                        </option>
                                        <option value="16:30:00" {{$in_time=='16:30:00' ? "selected" : "" }}>16:30:00
                                        </option>
                                        <option value="17:00:00" {{$in_time=='17:00:00' ? "selected" : "" }}>17:00:00
                                        </option>
                                        <option value="17:30:00" {{$in_time=='17:30:00' ? "selected" : "" }}>17:30:00
                                        </option>
                                        <option value="18:00:00" {{$in_time=='18:00:00' ? "selected" : "" }}>18:00:00
                                        </option>
                                        <option value="18:30:00" {{$in_time=='18:30:00' ? "selected" : "" }}>18:30:00
                                        </option>
                                        <option value="19:00:00" {{$in_time=='19:00:00' ? "selected" : "" }}>19:00:00
                                        </option>
                                        <option value="19:30:00" {{$in_time=='19:30:00' ? "selected" : "" }}>19:30:00
                                        </option>
                                        <option value="20:00:00" {{$in_time=='20:00:00' ? "selected" : "" }}>20:00:00
                                        </option>
                                        <option value="20:30:00" {{$in_time=='20:30:00' ? "selected" : "" }}>20:30:00
                                        </option>
                                        <option value="21:00:00" {{$in_time=='21:00:00' ? "selected" : "" }}>21:00:00
                                        </option>
                                        <option value="21:30:00" {{$in_time=='21:30:00' ? "selected" : "" }}>21:30:00
                                        </option>
                                        <option value="22:00:00" {{$in_time=='22:00:00' ? "selected" : "" }}>22:00:00
                                        </option>
                                        <option value="22:30:00" {{$in_time=='22:30:00' ? "selected" : "" }}>22:30:00
                                        </option>
                                        <option value="23:00:00" {{$in_time=='23:00:00' ? "selected" : "" }}>23:00:00
                                        </option>
                                        <option value="23:30:00" {{$in_time=='23:30:00' ? "selected" : "" }}>23:30:00
                                        </option>
                                    </select>
                                    @error('start_date')
                                    <div class="lara-error">
                                        {{$message}}
                                    </div>
                                    @enderror
                                    @error('in_time')
                                    <div class="lara-error">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col">
                                <label for="end_date" class="form-label">sluttdato og klokkeslett</label>
                                <div class="input-group mb-6">
                                    <span class="input-group-text" id="basic-addon2"><i
                                            class="icon-calendar"></i></span>
                                    <input type="text" class="form-control" id="end_date" name="end_date"
                                        placeholder="End Date" value="{{date("Y-m-d", strtotime($event->end_date))}}">
                                    <span class="input-group-text" id="basic-addon2"><i class="icon-time"></i></span>
                                    @php
                                    $out_time = Carbon\Carbon::parse($event->end_date)->format('H:i:s');
                                    @endphp
                                    <select class="form-select" name="out_time">
                                        <option value="00:00:00" {{$out_time =='00:00:00' ? "selected" : ""
                                            }}>00:00:00
                                        </option>
                                        <option value="00:30:00" {{$out_time =='00:30:00' ? "selected" : ""
                                            }}>00:30:00
                                        </option>
                                        <option value="01:00:00" {{$out_time =='01:00:00' ? "selected" : ""
                                            }}>01:00:00
                                        </option>
                                        <option value="01:30:00" {{$out_time =='01:30:00' ? "selected" : ""
                                            }}>01:30:00
                                        </option>
                                        <option value="02:00:00" {{$out_time =='02:00:00' ? "selected" : ""
                                            }}>02:00:00
                                        </option>
                                        <option value="02:30:00" {{$out_time =='02:30:00' ? "selected" : ""
                                            }}>02:30:00
                                        </option>
                                        <option value="03:00:00" {{$out_time =='03:00:00' ? "selected" : ""
                                            }}>03:00:00
                                        </option>
                                        <option value="03:30:00" {{$out_time =='03:30:00' ? "selected" : ""
                                            }}>03:30:00
                                        </option>
                                        <option value="04:00:00" {{$out_time =='04:00:00' ? "selected" : ""
                                            }}>04:00:00
                                        </option>
                                        <option value="04:30:00" {{$out_time =='04:30:00' ? "selected" : ""
                                            }}>04:30:00
                                        </option>
                                        <option value="05:00:00" {{$out_time =='05:00:00' ? "selected" : ""
                                            }}>05:00:00
                                        </option>
                                        <option value="05:30:00" {{$out_time =='05:30:00' ? "selected" : ""
                                            }}>05:30:00
                                        </option>
                                        <option value="06:00:00" {{$out_time =='06:00:00' ? "selected" : ""
                                            }}>06:00:00
                                        </option>
                                        <option value="06:30:00" {{$out_time =='06:30:00' ? "selected" : ""
                                            }}>06:30:00
                                        </option>
                                        <option value="07:00:00" {{$out_time =='07:00:00' ? "selected" : ""
                                            }}>07:00:00
                                        </option>
                                        <option value="07:30:00" {{$out_time =='07:30:00' ? "selected" : ""
                                            }}>07:30:00
                                        </option>
                                        <option value="08:00:00" {{$out_time =='08:00:00' ? "selected" : ""
                                            }}>08:00:00
                                        </option>
                                        <option value="08:30:00" {{$out_time =='08:30:00' ? "selected" : ""
                                            }}>08:30:00
                                        </option>
                                        <option value="09:00:00" {{$out_time =='09:00:00' ? "selected" : ""
                                            }}>09:00:00
                                        </option>
                                        <option value="09:30:00" {{$out_time =='09:30:00' ? "selected" : ""
                                            }}>09:30:00
                                        </option>
                                        <option value="10:00:00" {{$out_time =='10:00:00' ? "selected" : ""
                                            }}>10:00:00
                                        </option>
                                        <option value="10:30:00" {{$out_time =='10:30:00' ? "selected" : ""
                                            }}>10:30:00
                                        </option>
                                        <option value="11:00:00" {{$out_time =='11:00:00' ? "selected" : ""
                                            }}>11:00:00
                                        </option>
                                        <option value="11:30:00" {{$out_time =='11:30:00' ? "selected" : ""
                                            }}>11:30:00
                                        </option>
                                        <option value="12:00:00" {{$out_time =='12:00:00' ? "selected" : ""
                                            }}>12:00:00
                                        </option>
                                        <option value="12:30:00" {{$out_time =='12:30:00' ? "selected" : ""
                                            }}>12:30:00
                                        </option>
                                        <option value="13:00:00" {{$out_time =='13:00:00' ? "selected" : ""
                                            }}>13:00:00
                                        </option>
                                        <option value="13:30:00" {{$out_time =='13:30:00' ? "selected" : ""
                                            }}>13:30:00
                                        </option>
                                        <option value="14:00:00" {{$out_time =='14:00:00' ? "selected" : ""
                                            }}>14:00:00
                                        </option>
                                        <option value="14:30:00" {{$out_time =='14:30:00' ? "selected" : ""
                                            }}>14:30:00
                                        </option>
                                        <option value="15:00:00" {{$out_time =='15:00:00' ? "selected" : ""
                                            }}>15:00:00
                                        </option>
                                        <option value="15:30:00" {{$out_time =='15:30:00' ? "selected" : ""
                                            }}>15:30:00
                                        </option>
                                        <option value="16:00:00" {{$out_time =='16:00:00' ? "selected" : ""
                                            }}>16:00:00
                                        </option>
                                        <option value="16:30:00" {{$out_time =='16:30:00' ? "selected" : ""
                                            }}>16:30:00
                                        </option>
                                        <option value="17:00:00" {{$out_time =='17:00:00' ? "selected" : ""
                                            }}>17:00:00
                                        </option>
                                        <option value="17:30:00" {{$out_time =='17:30:00' ? "selected" : ""
                                            }}>17:30:00
                                        </option>
                                        <option value="18:00:00" {{$out_time =='18:00:00' ? "selected" : ""
                                            }}>18:00:00
                                        </option>
                                        <option value="18:30:00" {{$out_time =='18:30:00' ? "selected" : ""
                                            }}>18:30:00
                                        </option>
                                        <option value="19:00:00" {{$out_time =='19:00:00' ? "selected" : ""
                                            }}>19:00:00
                                        </option>
                                        <option value="19:30:00" {{$out_time =='19:30:00' ? "selected" : ""
                                            }}>19:30:00
                                        </option>
                                        <option value="20:00:00" {{$out_time =='20:00:00' ? "selected" : ""
                                            }}>20:00:00
                                        </option>
                                        <option value="20:30:00" {{$out_time =='20:30:00' ? "selected" : ""
                                            }}>20:30:00
                                        </option>
                                        <option value="21:00:00" {{$out_time =='21:00:00' ? "selected" : ""
                                            }}>21:00:00
                                        </option>
                                        <option value="21:30:00" {{$out_time =='21:30:00' ? "selected" : ""
                                            }}>21:30:00
                                        </option>
                                        <option value="22:00:00" {{$out_time =='22:00:00' ? "selected" : ""
                                            }}>22:00:00
                                        </option>
                                        <option value="22:30:00" {{$out_time =='22:30:00' ? "selected" : ""
                                            }}>22:30:00
                                        </option>
                                        <option value="23:00:00" {{$out_time =='23:00:00' ? "selected" : ""
                                            }}>23:00:00
                                        </option>
                                        <option value="23:30:00" {{$out_time =='23:30:00' ? "selected" : ""
                                            }}>23:30:00
                                        </option>
                                    </select>
                                    @error('end_date')
                                    <div class="lara-error">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="input-wrapper">
                        <div class="row">
                            <div class="col">
                                <label for="eventTitle" class="form-label">Maks antall deltakere</label>
                                <div class="input-group mb-6">
                                    <input type="number" class="form-control" name="max_participants" placeholder=""
                                        value="{{$event->max_participants}}">
                                    @error('max_participants')
                                    <div class="lara-error">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col">
                                <label for="eventTitle" class="form-label">Event Publish Date and Time</label>
                                <div class="input-group mb-6">
                                    <span class="input-group-text" id="basic-addon2"><i
                                            class="icon-calendar"></i></span>
                                    <input type="text" class="form-control" id="eventPublishDate"
                                        name="event_publish_date" placeholder="Publish Date" value="{{date("Y-m-d",
                                        strtotime($event->event_publish_date))}}">
                                    <span class="input-group-text" id="basic-addon2"><i class="icon-time"></i></span>
                                    @php
                                    $event_publish_time = Carbon\Carbon::parse($event->event_publish_date)->format('H:i:s');
                                    @endphp
                                    <select class="form-select" name="event_publish_time">
                                        <option value="00:00:00" {{$event_publish_time =='00:00:00' ? "selected"
                                            : ""
                                            }}>00:00:00</option>
                                        <option value="00:30:00" {{$event_publish_time =='00:30:00' ? "selected"
                                            : ""
                                            }}>00:30:00</option>
                                        <option value="01:00:00" {{$event_publish_time =='01:00:00' ? "selected"
                                            : ""
                                            }}>01:00:00</option>
                                        <option value="01:30:00" {{$event_publish_time =='01:30:00' ? "selected"
                                            : ""
                                            }}>01:30:00</option>
                                        <option value="02:00:00" {{$event_publish_time =='02:00:00' ? "selected"
                                            : ""
                                            }}>02:00:00</option>
                                        <option value="02:30:00" {{$event_publish_time =='02:30:00' ? "selected"
                                            : ""
                                            }}>02:30:00</option>
                                        <option value="03:00:00" {{$event_publish_time =='03:00:00' ? "selected"
                                            : ""
                                            }}>03:00:00</option>
                                        <option value="03:30:00" {{$event_publish_time =='03:30:00' ? "selected"
                                            : ""
                                            }}>03:30:00</option>
                                        <option value="04:00:00" {{$event_publish_time =='04:00:00' ? "selected"
                                            : ""
                                            }}>04:00:00</option>
                                        <option value="04:30:00" {{$event_publish_time =='04:30:00' ? "selected"
                                            : ""
                                            }}>04:30:00</option>
                                        <option value="05:00:00" {{$event_publish_time =='05:00:00' ? "selected"
                                            : ""
                                            }}>05:00:00</option>
                                        <option value="05:30:00" {{$event_publish_time =='05:30:00' ? "selected"
                                            : ""
                                            }}>05:30:00</option>
                                        <option value="06:00:00" {{$event_publish_time =='06:00:00' ? "selected"
                                            : ""
                                            }}>06:00:00</option>
                                        <option value="06:30:00" {{$event_publish_time =='06:30:00' ? "selected"
                                            : ""
                                            }}>06:30:00</option>
                                        <option value="07:00:00" {{$event_publish_time =='07:00:00' ? "selected"
                                            : ""
                                            }}>07:00:00</option>
                                        <option value="07:30:00" {{$event_publish_time =='07:30:00' ? "selected"
                                            : ""
                                            }}>07:30:00</option>
                                        <option value="08:00:00" {{$event_publish_time =='08:00:00' ? "selected"
                                            : ""
                                            }}>08:00:00</option>
                                        <option value="08:30:00" {{$event_publish_time =='08:30:00' ? "selected"
                                            : ""
                                            }}>08:30:00</option>
                                        <option value="09:00:00" {{$event_publish_time =='09:00:00' ? "selected"
                                            : ""
                                            }}>09:00:00</option>
                                        <option value="09:30:00" {{$event_publish_time =='09:30:00' ? "selected"
                                            : ""
                                            }}>09:30:00</option>
                                        <option value="10:00:00" {{$event_publish_time =='10:00:00' ? "selected"
                                            : ""
                                            }}>10:00:00</option>
                                        <option value="10:30:00" {{$event_publish_time =='10:30:00' ? "selected"
                                            : ""
                                            }}>10:30:00</option>
                                        <option value="11:00:00" {{$event_publish_time =='11:00:00' ? "selected"
                                            : ""
                                            }}>11:00:00</option>
                                        <option value="11:30:00" {{$event_publish_time =='11:30:00' ? "selected"
                                            : ""
                                            }}>11:30:00</option>
                                        <option value="12:00:00" {{$event_publish_time =='12:00:00' ? "selected"
                                            : ""
                                            }}>12:00:00</option>
                                        <option value="12:30:00" {{$event_publish_time =='12:30:00' ? "selected"
                                            : ""
                                            }}>12:30:00</option>
                                        <option value="13:00:00" {{$event_publish_time =='13:00:00' ? "selected"
                                            : ""
                                            }}>13:00:00</option>
                                        <option value="13:30:00" {{$event_publish_time =='13:30:00' ? "selected"
                                            : ""
                                            }}>13:30:00</option>
                                        <option value="14:00:00" {{$event_publish_time =='14:00:00' ? "selected"
                                            : ""
                                            }}>14:00:00</option>
                                        <option value="14:30:00" {{$event_publish_time =='14:30:00' ? "selected"
                                            : ""
                                            }}>14:30:00</option>
                                        <option value="15:00:00" {{$event_publish_time =='15:00:00' ? "selected"
                                            : ""
                                            }}>15:00:00</option>
                                        <option value="15:30:00" {{$event_publish_time =='15:30:00' ? "selected"
                                            : ""
                                            }}>15:30:00</option>
                                        <option value="16:00:00" {{$event_publish_time =='16:00:00' ? "selected"
                                            : ""
                                            }}>16:00:00</option>
                                        <option value="16:30:00" {{$event_publish_time =='16:30:00' ? "selected"
                                            : ""
                                            }}>16:30:00</option>
                                        <option value="17:00:00" {{$event_publish_time =='17:00:00' ? "selected"
                                            : ""
                                            }}>17:00:00</option>
                                        <option value="17:30:00" {{$event_publish_time =='17:30:00' ? "selected"
                                            : ""
                                            }}>17:30:00</option>
                                        <option value="18:00:00" {{$event_publish_time =='18:00:00' ? "selected"
                                            : ""
                                            }}>18:00:00</option>
                                        <option value="18:30:00" {{$event_publish_time =='18:30:00' ? "selected"
                                            : ""
                                            }}>18:30:00</option>
                                        <option value="19:00:00" {{$event_publish_time =='19:00:00' ? "selected"
                                            : ""
                                            }}>19:00:00</option>
                                        <option value="19:30:00" {{$event_publish_time =='19:30:00' ? "selected"
                                            : ""
                                            }}>19:30:00</option>
                                        <option value="20:00:00" {{$event_publish_time =='20:00:00' ? "selected"
                                            : ""
                                            }}>20:00:00</option>
                                        <option value="20:30:00" {{$event_publish_time =='20:30:00' ? "selected"
                                            : ""
                                            }}>20:30:00</option>
                                        <option value="21:00:00" {{$event_publish_time =='21:00:00' ? "selected"
                                            : ""
                                            }}>21:00:00</option>
                                        <option value="21:30:00" {{$event_publish_time =='21:30:00' ? "selected"
                                            : ""
                                            }}>21:30:00</option>
                                        <option value="22:00:00" {{$event_publish_time =='22:00:00' ? "selected"
                                            : ""
                                            }}>22:00:00</option>
                                        <option value="22:30:00" {{$event_publish_time =='22:30:00' ? "selected"
                                            : ""
                                            }}>22:30:00</option>
                                        <option value="23:00:00" {{$event_publish_time =='23:00:00' ? "selected"
                                            : ""
                                            }}>23:00:00</option>
                                        <option value="23:30:00" {{$event_publish_time =='23:30:00' ? "selected"
                                            : ""
                                            }}>23:30:00</option>
                                    </select>
                                    @error('event_publish_date')
                                    <div class="lara-error">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="input-wrapper">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="eventTitle" class="form-label">Gateadresse</label>
                                <div class="input-group mb-6">
                                    <input type="text" class="form-control" name="street_address"
                                        placeholder="Street Address" value="{{$event->street_address}}">
                                </div>
                                @error('street_address')
                                <div class="lara-error">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label for="eventTitle" class="form-label">By</label>
                                <div class="input-group mb-6">
                                    <input type="text" class="form-control" name="city" placeholder="City"
                                        value="{{$event->city}}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="input-wrapper">
                        <label for="event_form_location" class="form-label">Arrangementets sted</label>
                        <div class="input-group mb-6">
                            <input type="text" class="form-control" id="event_form_location" name="event_form_location"
                                placeholder="" value="{{$event->event_form_location}}">
                        </div>
                    </div>
                    <div class="input-wrapper">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="event_form_featured" class="form-label label-block">Vis som
                                    topp-arrangement</label>
                                <div class="form-check form-switch">
                                    <input class="form-check-input" type="checkbox" name="event_form_featured"
                                        id="event_form_featured" value="1" {{$event->event_form_featured ==1 ? "checked"
                                    :
                                    ""
                                    }}>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="split_price" class="form-label label-block">Show price and fee.</label>
                                <div class="form-check form-switch">
                                    <input class="form-check-input" type="checkbox" id="split_price" name="split_price"
                                        value="1" {{$event->split_price ==1 ? "checked" : "" }}>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="input-wrapper payment-inputs">
                        <label class="form-label label-block">Betalingsmåter</label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payment_type" id="payment_type1"
                                value="0" {{$event->payment_type == 0 ? "checked" : "" }}>
                            <label class="form-check-label" for="payment_type1">Online betaling</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payment_type" id="payment_type2"
                                value="1" {{$event->payment_type == 1 ? "checked" : "" }}>
                            <label class="form-check-label" for="payment_type2">Faktura</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payment_type" id="payment_type3"
                                value="2" {{$event->payment_type == 2 ? "checked" : "" }}>
                            <label class="form-check-label" for="payment_type3">Begge</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payment_type" id="payment_type4"
                                value="3" {{$event->payment_type == 2 ? "checked" : "" }}>
                            <label class="form-check-label" for="payment_type4">Free</label>
                        </div>
                    </div>
                    <div class="input-wrapper ehf_invoice_inpu" style="{{$event->is_ehf_invoice == 1 ? "" : " display:
                        none" }}">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="1" name="is_ehf_invoice"
                                id="is_ehf_invoice" {{$event->is_ehf_invoice == 1 ? "checked" : "" }}>
                            <label class="form-check-label" for="is_ehf_invoice">
                                Is EHF Invoice:
                            </label>
                        </div>
                    </div>
                    <div class="input-wrapper">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="event_form_vat" class="form-label label-block">Mva</label>
                                <div class="form-check form-switch">
                                    <input class="form-check-input" name="event_form_vat" type="checkbox"
                                        id="event_form_vat" value="1" {{$event->event_form_vat ==1 ? "checked" : "" }}>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="event_form_send_ticket" class="form-label label-block">Send billett</label>
                                <div class="form-check form-switch">
                                    <input class="form-check-input" name="event_form_send_ticket" type="checkbox"
                                        id="event_form_send_ticket" value="1" {{$event->event_form_send_ticket ==1 ?
                                    "checked" : "" }}>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="input-wrapper">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="invoice_fee" class="form-label">Fakturagebyr</label>
                                <div class="input-group mb-6">
                                    <input type="text" class="form-control" id="invoice_fee" name="invoice_fee"
                                        placeholder="100" value="{{$event->invoice_fee}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="due_date" class="form-label">Forfallsdato: Antall dager før arrangementet
                                    skal
                                    avholdes.</label>
                                <div class="input-group mb-6">
                                    <input type="text" class="form-control" id="due_date" name="due_date"
                                        placeholder="Deu Date" value="{{$event->due_date}}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="input-wrapper">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="invoice_due_date_text_flag" class="form-label label-block">Egen tekst til
                                    forfallsdato</label>
                                <div class="form-check form-switch">
                                    <input class="form-check-input" type="checkbox" id="invoice_due_date_text_flag"
                                        name="invoice_due_date_text_flag" value="1" {{$event->invoice_due_date_text_flag
                                    ==1
                                    ? "checked" : "" }}>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="add_instruction" class="form-label label-block">I want to send instruction
                                    for
                                    this event.</label>
                                <div class="form-check form-switch">
                                    <input class="form-check-input" type="checkbox" id="add_instruction"
                                        name="add_instruction" value="1" {{$event->add_instruction ==1 ? "checked" : ""
                                    }}>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="input-wrapper">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="invoice_account_no" class="form-label">Kontonummer på faktura</label>
                                <div class="input-group mb-6">
                                    <input type="text" class="form-control" id="invoice_account_no"
                                        name="invoice_account_no" placeholder="Invoice Account number"
                                        value="{{$event->invoice_account_no}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="no_registration" class="form-label label-block">No registration for this
                                    event.</label>
                                <div class="form-check form-switch">
                                    <input class="form-check-input" type="checkbox" name="no_registration"
                                        id="no_registration" value="1" {{$event->no_registration ==1 ? "checked" : ""
                                    }}>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-btn">
                        <button type="submit" class="btn btn-primary">Update Event</button>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="input-wrapper">
                        <label for="formFileMultiple" class="form-label">Last opp bilde. Størrelse: 484Bx478H piksler.
                            72
                            dpi.
                            Vær oppmerksom på at nederste del blir dekket av tekst.</label>
                        <div class="image-wrapper">
                            <img src="{{asset('storage/upload/'.$event->image)}}" id="previewImg" alt="preview Image">
                        </div>
                    </div>
                    <div class="input-wrapper">
                        <div class="mb-3">
                            <label for="uploadImage" class="form-label">Gyldige filformater: jpg, png og gif</label>
                            <input class="form-control" type="file" id="uploadImage" name="image"
                                onchange="previewFile(this);">
                            <input class="form-control" type="hidden" name="imageedit" value="{{$event->image}}">
                            @error('image')
                            <div class="lara-error">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script src="{{asset('packeg/ckeditor5/ckeditor.js')}}"></script>
<script>
    ClassicEditor
		.create( document.querySelector( '.cktextarea' ), {
			// toolbar: [ 'heading', '|', 'bold', 'italic', 'link' ]
		} )
		.then( editor => {
			window.editor = editor;
		} )
		.catch( err => {
			console.error( err.stack );
		} );
        function previewFile(input){
        var file = $("#uploadImage").get(0).files[0];
        if(file){
            var reader = new FileReader();
            reader.onload = function(){
                $("#previewImg").attr("src", reader.result);
            }
            reader.readAsDataURL(file);
        }
    }
</script>
@endsection
