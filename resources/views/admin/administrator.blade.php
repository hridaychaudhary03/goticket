@extends('admin.main')
@section('main-section')
<div class="inner-heading">
    <h1 class="page-header-title"><i class="icon-flag-alt"></i>Administrator</h1>
</div>
<div class="breadcrumb-wrapper">
    <ul class="breadcrumb-lists">
        <li class="bread-item"><a href="{{url('admin/dashboard')}}">Dashboard</a> <i class="icon-angle-right"></i></li>
        <li class="bread-item active"><a href="#">Administrator</a></li>
    </ul>
</div>
<div class="inner-wrapper">
    @if(session()->has('message'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{session()->get('message')}}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    <div class="card">
        <div class="table-responsive table-billing-history">
            <table class="table mb-0">
                <thead>
                    <tr>
                        <th class="border-gray-200 text-center" scope="col">SN</th>
                        <th class="border-gray-200 text-center" scope="col">Name</th>
                        <th class="border-gray-200 text-center" scope="col">Email</th>
                        <th class="border-gray-200 text-center" scope="col">Contract</th>
                        <th class="border-gray-200 text-center" scope="col">Address</th>
                        <th class="border-gray-200 text-center" scope="col">Org Number</th>
                        <th class="border-gray-200 text-center" scope="col">Bank Account</th>
                        <th class="border-gray-200 text-center" scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $key => $user)
                    <tr>
                        <td class="align-middle text-center">{{$key}}</td>
                        <td class="align-middle text-center">{{$user->name}}</td>
                        <td class="align-middle text-center">{{$user->email}}</td>
                        <td class="align-middle text-center">{{$user->phone}}</td>
                        <td class="align-middle text-center">{{$user->address}}</td>
                        <td class="align-middle text-center">{{$user->org_number ?? "-"}}</td>
                        <td class="align-middle text-center">{{$user->bank_account ?? "-"}}</td>
                        <td class="align-middle text-center">
                            <a href="#" class="btn btn-primary"><i class="icon-edit"></i></a>
                            <a href="#" class="btn btn-danger confirm-alert"
                                data-message="Are you sure to delete this user !"><i class="icon-trash"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
