<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>Goticket | We Play Along - Administrator List</title>
    <link rel="icon" href="{!! asset('img/goticket-logo.png') !!}">
    <link href="{{asset('fontawesome-6.2.1/css/all.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/css/bootstrap.min.css')}}" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="{{asset('admin/css/datepicker.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('admin/css/main.css')}}">

</head>

<body>
    <div id="admin-wrapper">
        <div class="admin-top-bar">
            <div class="admin-brand">
                <a href="{{url('admin/dashboard')}}"
                    class="text-white text-decoration-none">
                    {{$siteGeneralSettings->site_name}}
                </a>
            </div>
            <div class="text-end avtar-section">
                <div class="avtar-wrapper">
                    <span class="small">Welcome : {{
                        Auth::user()->name }}</span>
                    <img class="img-profile rounded-circle" src="{{asset('img/undraw_profile.svg')}}">
                    <i class="icon-angle-down"></i>
                </div>
                <div class="avtar-dropdown">
                    <a href="{{ route('profile.edit') }}" class="avtar-dropdown-text"><i class="fas fa-user-circle"></i> profiile</a>
                    <form method="POST" action="{{ route('logout') }}">
                        @csrf
                        <a href="{{url('/logout')}}" class="avtar-dropdown-text" onclick="event.preventDefault();
                        this.closest('form').submit();"><i class="fas fa-sign-out-alt"></i> Log Out</a>
                    </form>
                </div>
            </div>
        </div>
        <div class="nav-sidebar">
            <ul class="shortcut-nav">
                <li><a href="#"><i class="icon-laptop"></i></a></li>
                <li><a href="#"><i class="icon-cogs"></i></a></li>
                <li><a href="#"><i class="icon-file-text"></i></a></li>
                <li><a href="#"><i class="icon-flag"></i></a></li>
            </ul>
            <ul class="admin-navigation">
                <li class="nav-item {{request()->is('admin/dashboard') ? 'active':''}}"><a
                        href="{{url('admin/dashboard')}}"> <i class="icon-dashboard"></i> Dashboard</a></li>
                <li class="nav-item {{request()->is('admin/users') ? 'active open':''}}"><a
                        href="{{url('admin/users')}}"> <i class="icon-laptop"></i> Administrator</a>
                </li>
                <li class="nav-item has-dropdown {{request()->is('admin/account/*') ? 'active':''}}">
                    <a href="#"><i class="icon-wrench"></i>Account <i class="menu-arrow icon-angle-down"></i></a>
                    <ul class="dropdown">
                        <li><a href="#">Account</a></li>
                        <li><a href="#">Account</a></li>
                        <li><a href="#">Account</a></li>
                        <li><a href="#">Account</a></li>
                        <li><a href="#">Account</a></li>
                    </ul>
                </li>
                <li class="nav-item has-dropdown {{request()->is('admin/event/*') ? 'active open':''}}">
                    <a href="#"><i class="icon-tasks"></i> Event Management<i
                            class="menu-arrow icon-angle-down"></i></a>
                    <ul class="dropdown">
                        <li class="{{request()->is('admin/event/event-list') ? 'active':''}}"><a
                                href="{{url('admin/event/event-list')}}">Active Events</a></li>
                        <li class="{{request()->is('admin/event/event-trashed') ? 'active':''}}"><a
                                href="{{route('admin.trashedEvent')}}">Trashed Events</a></li>
                        <li class="{{request()->is('admin/event/event-cancel-list') ? 'active':''}}"><a href="{{route('admin.cancelEventList')}}">Cancel Events</a></li>
                        <li><a href="#">Account</a></li>
                        <li><a href="#">Account</a></li>
                    </ul>
                </li>
                {{-- <li><a href="#"><i class="icon-tasks"></i> Event Management</a></li> --}}
                <li><a href="#"><i class="icon-ticket"></i>Ticket Orders</a></li>
                <li><a href="#"><i class="icon-user"></i>Client Info</a></li>
                <li class="nav-item has-dropdown">
                    <a href="#"><i class="icon-user"></i>User management <i class="menu-arrow icon-angle-down"></i> </a>
                    <ul class="dropdown">
                        <li><a href="#">Account</a></li>
                        <li><a href="#">Account</a></li>
                        <li><a href="#">Account</a></li>
                        <li><a href="#">Account</a></li>
                        <li><a href="#">Account</a></li>
                    </ul>
                </li>
                <li><a href="#"><i class="icon-credit-card"></i>Payment Option</a></li>
                <li class="nav-item has-dropdown {{request()->is('admin/settings/*') ? 'active open':''}}">
                    <a href="#"><i class="icon-cogs"></i>Site Settings<i class="menu-arrow icon-angle-down"></i> </a>
                    <ul class="dropdown">
                        <li class="{{request()->is('admin/settings/general') ? 'active':''}}"><a
                                href="{{url('admin/settings/general')}}">General</a></li>
                        <li><a href="#">Account</a></li>
                        <li><a href="#">Account</a></li>
                        <li><a href="#">Account</a></li>
                        <li><a href="#">Account</a></li>
                    </ul>
                </li>
                <li class="nav-item {{request()->is('admin/time-zone') ? 'active open':''}}"><a
                        href="{{url('admin/time-zone')}}"><i class="icon-time"></i>Time Zone Settings</a></li>
                <li><a href="#"><i class="icon-list"></i>Mail Logs</a></li>
                <li><a href="#"><i class="icon-usd"></i>Tripletex</a></li>
            </ul>
        </div>
