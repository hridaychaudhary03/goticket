@extends('admin.main')
@section('main-section')
<div class="inner-heading">
    <h1 class="page-header-title"><i class="icon-flag-alt"></i>Admin dashboard</h1>
</div>
<div class="breadcrumb-wrapper">
    <ul class="breadcrumb-lists">
        <li class="bread-item"><a href="#">Admin</a> <i class="icon-angle-right"></i></li>
        <li class="bread-item active"><a href="#">Dashboard</a></li>
    </ul>
</div>
@endsection
