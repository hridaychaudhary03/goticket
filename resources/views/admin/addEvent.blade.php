@extends('admin.main')
@section('main-section')
<div class="inner-heading">
    <h1 class="page-header-title"><i class="icon-flag-alt"></i>Create Event</h1>
    <div class="event-btn-group">
        <a href="{{route('admin.eventlists')}}" class="btn btn-primary">Active Events</a>
    </div>
</div>
<div class="breadcrumb-wrapper">
    <ul class="breadcrumb-lists">
        <li class="bread-item"><a href="{{url('admin/dashboard')}}">Dashboard</a> <i class="icon-angle-right"></i></li>
        <li class="bread-item active"><a href="#">New Event</a></li>
    </ul>
</div>
<div class="inner-wrapper">
    @include('admin.tools.eventInnerNav')
    <form action="{{url('admin/event/event-store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="input-wrapper">
                        <label for="eventTitle" class="form-label">Title</label>
                        <input type="text" class="form-control" name="title" id="eventTitle" placeholder="Title"
                            value="{{old('title')}}">
                        @error('title')
                        <div class="lara-error">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="input-wrapper">
                        <label for="description" class="form-label">Tekst til arrangementet</label>
                        <textarea class="form-control cktextarea" name="description" id="description" rows="3">
                        {{old('description')}}
                    </textarea>
                    </div>
                    <div class="input-wrapper">
                        <div class="row">
                            <div class="col">
                                <label for="start_date" class="form-label">startdato og klokkeslett</label>
                                <div class="input-group mb-6">
                                    <span class="input-group-text" id="basic-addon2"><i
                                            class="icon-calendar"></i></span>
                                    <input type="text" class="form-control" id="start_date" name="start_date"
                                        placeholder="Start Date" value="{{old('start_date')}}">
                                    <span class="input-group-text" id="basic-addon2"><i class="icon-time"></i></span>
                                    <select class="form-select" name="in_time" id="in_time"
                                        data-selected="{{old('in_time')}}">
                                    </select>
                                    @error('start_date')
                                    <div class="lara-error">
                                        {{$message}}
                                    </div>
                                    @enderror
                                    @error('in_time')
                                    <div class="lara-error">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col">
                                <label for="end_date" class="form-label">sluttdato og klokkeslett</label>
                                <div class="input-group mb-6">
                                    <span class="input-group-text" id="basic-addon2"><i
                                            class="icon-calendar"></i></span>
                                    <input type="text" class="form-control" id="end_date" name="end_date"
                                        placeholder="End Date" value="{{old('end_date')}}">
                                    <span class="input-group-text" id="basic-addon2"><i class="icon-time"></i></span>
                                    <select class="form-select" name="out_time" id="out_time"
                                        data-selected="{{old('out_time')}}">
                                    </select>
                                    @error('end_date')
                                    <div class="lara-error">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="input-wrapper">
                        <div class="row">
                            <div class="col">
                                <label for="eventTitle" class="form-label">Maks antall deltakere</label>
                                <div class="input-group mb-6">
                                    <input type="number" class="form-control" name="max_participants" placeholder=""
                                        value="{{old('max_participants')}}">
                                    @error('max_participants')
                                    <div class="lara-error">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col">
                                <label for="eventTitle" class="form-label">Event Publish Date and Time</label>
                                <div class="input-group mb-6">
                                    <span class="input-group-text" id="basic-addon2"><i
                                            class="icon-calendar"></i></span>
                                    <input type="text" class="form-control" id="eventPublishDate"
                                        name="event_publish_date" placeholder="Publish Date"
                                        value="{{old('event_publish_date')}}">
                                    <span class="input-group-text" id="basic-addon2"><i class="icon-time"></i></span>
                                    <select class="form-select" name="event_publish_time" id="event_publish_time"
                                        data-selected="{{old('event_publish_time')}}">
                                    </select>
                                    @error('event_publish_date')
                                    <div class="lara-error">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="input-wrapper">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="eventTitle" class="form-label">Gateadresse</label>
                                <div class="input-group mb-6">
                                    <input type="text" class="form-control" name="street_address"
                                        placeholder="Street Address" value="{{old('street_address')}}">
                                </div>
                                @error('street_address')
                                <div class="lara-error">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label for="eventTitle" class="form-label">By</label>
                                <div class="input-group mb-6">
                                    <input type="text" class="form-control" name="city" placeholder="City"
                                        value="{{old('city')}}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="input-wrapper">
                        <label for="event_form_location" class="form-label">Arrangementets sted</label>
                        <div class="input-group mb-6">
                            <input type="text" class="form-control" id="event_form_location" name="event_form_location"
                                placeholder="" value="{{old('event_form_location')}}">
                        </div>
                    </div>
                    <div class="input-wrapper">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="event_form_featured" class="form-label label-block">Vis som
                                    topp-arrangement</label>
                                <div class="form-check form-switch">
                                    <input class="form-check-input" type="checkbox" name="event_form_featured"
                                        id="event_form_featured" value="1" {{old('event_form_featured')==1 ? "checked"
                                        : "" }}>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="split_price" class="form-label label-block">Show price and fee.</label>
                                <div class="form-check form-switch">
                                    <input class="form-check-input" type="checkbox" id="split_price" name="split_price"
                                        value="1" {{old('split_price')==1 ? "checked" : "" }}>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="input-wrapper payment-inputs">
                        <label class="form-label label-block">Betalingsmåter</label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payment_type" id="payment_type1"
                                value="0" checked>
                            <label class="form-check-label" for="payment_type1">Online betaling</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payment_type" id="payment_type2"
                                value="1">
                            <label class="form-check-label" for="payment_type2">Faktura</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payment_type" id="payment_type3"
                                value="2">
                            <label class="form-check-label" for="payment_type3">Begge</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payment_type" id="payment_type4"
                                value="3">
                            <label class="form-check-label" for="payment_type4">Free</label>
                        </div>
                    </div>
                    <div class="input-wrapper ehf_invoice_inpu" style="display: none">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="1" name="is_ehf_invoice"
                                id="is_ehf_invoice" {{old('is_ehf_invoice')==1 ? "checked" : "" }}>
                            <label class="form-check-label" for="is_ehf_invoice">
                                Is EHF Invoice:
                            </label>
                        </div>
                    </div>
                    <div class="input-wrapper">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="event_form_vat" class="form-label label-block">Mva</label>
                                <div class="form-check form-switch">
                                    <input class="form-check-input" name="event_form_vat" type="checkbox"
                                        id="event_form_vat" value="1" {{old('event_form_vat')==1 ? "checked" : "" }}>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="event_form_send_ticket" class="form-label label-block">Send billett</label>
                                <div class="form-check form-switch">
                                    <input class="form-check-input" name="event_form_send_ticket" type="checkbox"
                                        id="event_form_send_ticket" value="1" checked>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="input-wrapper">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="invoice_fee" class="form-label">Fakturagebyr</label>
                                <div class="input-group mb-6">
                                    <input type="text" class="form-control" id="invoice_fee" placeholder="100"
                                        value="{{old('event_form_vat')}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="due_date" class="form-label">Forfallsdato: Antall dager før arrangementet
                                    skal
                                    avholdes.</label>
                                <div class="input-group mb-6">
                                    <input type="text" class="form-control" id="due_date" name="due_date"
                                        placeholder="Deu Date" {{old('due_date')==1 ? "checked" : "" }}>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="input-wrapper">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="invoice_due_date_text_flag" class="form-label label-block">Egen tekst til
                                    forfallsdato</label>
                                <div class="form-check form-switch">
                                    <input class="form-check-input" type="checkbox" id="invoice_due_date_text_flag"
                                        name="invoice_due_date_text_flag" value="1">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="add_instruction" class="form-label label-block">I want to send instruction
                                    for
                                    this event.</label>
                                <div class="form-check form-switch">
                                    <input class="form-check-input" type="checkbox" id="add_instruction" value="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="input-wrapper">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="invoice_account_no" class="form-label">Kontonummer på faktura</label>
                                <div class="input-group mb-6">
                                    <input type="text" class="form-control" id="invoice_account_no"
                                        name="invoice_account_no" placeholder="Invoice Account number">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="no_registration" class="form-label label-block">No registration for this
                                    event.</label>
                                <div class="form-check form-switch">
                                    <input class="form-check-input" type="checkbox" name="no_registration"
                                        id="no_registration" value="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-btn">
                        <button type="submit" class="btn btn-primary">Create New Event</button>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="input-wrapper">
                        <label for="formFileMultiple" class="form-label">Last opp bilde. Størrelse: 484Bx478H piksler.
                            72
                            dpi.
                            Vær oppmerksom på at nederste del blir dekket av tekst.</label>
                        <label class="image-wrapper" for="uploadImage">
                            <img src="{{asset('img/image-placeholder.png')}}" id="previewImg" alt="preview Image">
                        </label>
                    </div>
                    <div class="input-wrapper">
                        <div class="mb-3">
                            <label for="uploadImage" class="form-label">Gyldige filformater: jpg, png og gif</label>
                            <input class="form-control" type="file" id="uploadImage" name="image"
                                onchange="previewFile(this);">
                            @error('image')
                            <div class="lara-error">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script src="{{asset('packeg/ckeditor5/ckeditor.js')}}"></script>
<script>
    ClassicEditor
		.create( document.querySelector( '.cktextarea' ), {
			// toolbar: [ 'heading', '|', 'bold', 'italic', 'link' ]
		} )
		.then( editor => {
			window.editor = editor;
		} )
		.catch( err => {
			console.error( err.stack );
		} );
        function previewFile(input){
        var file = $("#uploadImage").get(0).files[0];
        if(file){
            var reader = new FileReader();
            reader.onload = function(){
                $("#previewImg").attr("src", reader.result);
            }
            reader.readAsDataURL(file);
        }
    }
</script>
@endsection
