@extends('admin.main')
@section('main-section')
<div class="inner-heading">
    <h1 class="page-header-title"><i class="icon-flag-alt"></i>Cancel Event Lists</h1>
    <div class="event-btn-group">
        <a href="{{route('admin.trashedEvent')}}" class="btn btn-danger">Trashed Events</a>
        <a href="{{route('admin.eventlists')}}" class="btn btn-success">Active Events</a>
    </div>
</div>
<div class="breadcrumb-wrapper">
    <ul class="breadcrumb-lists">
        <li class="bread-item"><a href="{{url('admin/dashboard')}}">Dashboard</a> <i class="icon-angle-right"></i></li>
        <li class="bread-item active"><a href="#">Cancel Event Lists</a></li>
    </ul>
</div>
<div class="inner-wrapper">
    @if(session()->has('message'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{session()->get('message')}}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    <div class="card">
        <div class="table-responsive table-billing-history">
            <table class="table mb-0">
                <thead>
                    <tr>
                        <th class="border-gray-200 text-center" scope="col">Image</th>
                        <th class="border-gray-200 text-center" scope="col">Event name</th>
                        <th class="border-gray-200 text-center" scope="col">Event Date</th>
                        <th class="border-gray-200 text-center" scope="col">Client Info</th>
                        <th class="border-gray-200 text-center" scope="col">Created By</th>
                        <th class="border-gray-200 text-center" scope="col">Status</th>
                        <th class="border-gray-200 text-center" scope="col">Tickets sold</th>
                        <th class="border-gray-200 text-center" scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($Events as $key => $Event)
                    <tr>
                        <td class="align-middle text-center"><a style="width: 100px; display:inline-block;"><img
                                    width="100%" src="{{asset('storage/upload/'.$Event->image)}}" alt=""></a></td>
                        <td class="align-middle text-center">{{$Event->title}}</td>
                        <td class="align-middle text-center">
                            {{ Carbon\Carbon::parse($Event->start_date)->format('d M y') }}
                        </td>
                        <td class="align-middle text-center">
                            <a class="btn btn-primary" data-bs-toggle="tooltip" data-bs-placement="top"
                                title="List User">
                                <i class="icon-user"></i>
                            </a>
                        </td>
                        <td class="align-middle text-center">Goticket</td>
                        <td class="align-middle text-center"><span class="badge bg-success">{{$Event->status}}</span>
                        </td>
                        <td class="align-middle text-center">0 (MAX: {{$Event->max_participants}}) <i
                                class="icon-ticket" style="color: #F16184;"></i></td>
                        <td class="align-middle text-center">
                            <a href="{{route('admin.editEvent', ['id'=>$Event->id])}}" class="btn btn-primary"
                                data-bs-toggle="tooltip" data-bs-placement="top" title="Edit">
                                <i class="icon-edit"></i>
                            </a>
                            <a href="{{route('admin.processingEvent', ['id'=>$Event->id])}}" class="btn btn-info"
                                data-bs-toggle="tooltip" data-bs-placement="top" title="process">
                                <i class="fas fa-microchip"></i>
                            </a>
                            <a href="{{route('admin.trashEvent', ['id'=>$Event->id])}}"
                                class="btn btn-danger confirm-alert" data-message="Confirm Event Move to trash"
                                data-bs-toggle="tooltip" data-bs-placement="top" title="trash">
                                <i class="icon-trash"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
