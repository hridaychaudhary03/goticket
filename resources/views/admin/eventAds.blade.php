@extends('admin.main')
@section('main-section')
@include('admin.tools.flashmessage')
<div class="inner-heading">
    <h1 class="page-header-title"><i class="icon-flag-alt"></i>Advertiesment</h1>
</div>
<div class="breadcrumb-wrapper">
    <ul class="breadcrumb-lists">
        <li class="bread-item"><a href="{{url('admin/dashboard')}}">Dashboard</a> <i class="icon-angle-right"></i></li>
        <li class="bread-item active"><a href="#">New Ticket Category</a></li>
    </ul>
</div>
<div class="inner-wrapper">
    <div class="tCategory-wrapper">
        <div class="card">
            @include('admin.tools.eventInnerNav')
            <form action="{{url('admin/event/createOrUpdate-advertiesment')}}" method="POST"
                enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="event_id" value="{{$id}}">
                <div class="form-row" id="main-advs">
                    <div class="adsController card">
                        <input type="radio" name="advs" id="advs1" value="1">
                        <label for="advs1">
                            <img src="{{asset('img/advs1.png')}}" alt="">
                        </label>
                    </div>
                    <div class="adsController card">
                        <input type="radio" name="advs" id="advs2" value="2">
                        <label for="advs2">
                            <img src="{{asset('img/advs2.png')}}" alt="">
                        </label>
                    </div>
                    <div class="adsController card">
                        <input type="radio" name="advs" id="advs3" value="2">
                        <label for="advs3">
                            <img src="{{asset('img/advs3.png')}}" alt="">
                        </label>
                    </div>
                    <div class="adsController card">
                        <input type="radio" name="advs" id="advs4" value="4">
                        <label for="advs4">
                            <img src="{{asset('img/advs4.png')}}" alt="">
                        </label>
                    </div>
                    <div class="adsController card">
                        <input type="radio" name="advs" id="advs5" value="3">
                        <label for="advs5">
                            <img src="{{asset('img/advs5.png')}}" alt="">
                        </label>
                    </div>
                    <div class="adsController card">
                        <input type="radio" name="advs" id="advs6" value="3">
                        <label for="advs6">
                            <img src="{{asset('img/advs6.png')}}" alt="">
                        </label>
                    </div>
                    <div class="adsController card active">
                        <input type="radio" name="advs" id="advs7" value="0" checked>
                        <label for="advs7">
                            <img src="{{asset('img/advs7.png')}}" alt="">
                        </label>
                    </div>
                </div>
                <div class="form-row mt-4" id="main-ads-img" data-src="{{asset('img/image-placeholder.png')}}">
                </div>
                <input type="submit" value="Click">
            </form>
        </div>
    </div>
</div>
</div>
@endsection
