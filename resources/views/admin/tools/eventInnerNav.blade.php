<div class="row event-nav">
    <div class="col">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link {{request()->is('admin/event/add-event') ? 'active':''}}" aria-current="page"
                    href="#">Information</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{request()->is('admin/event/ticket-category/*') ? 'active':''}}"
                    href="#">Tickets</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{request()->is('admin/event/event-advertiesment/*') ? 'active':''}}" href="#">Advertiesment</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Preview</a>
            </li>
        </ul>
    </div>
</div>
