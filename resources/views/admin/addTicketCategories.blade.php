@extends('admin.main')
@section('main-section')
@include('admin.tools.flashmessage')
<div class="inner-heading">
    <h1 class="page-header-title"><i class="icon-flag-alt"></i>Ticket Category</h1>
</div>
<div class="breadcrumb-wrapper">
    <ul class="breadcrumb-lists">
        <li class="bread-item"><a href="{{url('admin/dashboard')}}">Dashboard</a> <i class="icon-angle-right"></i></li>
        <li class="bread-item active"><a href="#">New Ticket Category</a></li>
    </ul>
</div>
<div class="inner-wrapper">
    <div class="tCategory-wrapper">
        <div class="card">
            @include('admin.tools.eventInnerNav')
            @if($categories->count() > 0)
            @foreach($categories as $key => $categorie)
            <div class="tCategory-item">
                <div class="tCategory-item-wrapper">
                    <div class="tCategory ticket-section">
                        <div class="category-form">
                            <form action="#">
                                @php
                                $calculated_ticket_retail_price = $categorie->price + $categorie->fee;
                                @endphp
                                <input type="hidden" name="id" value="{{$categorie->id}}">
                                <input type="hidden" name="event_id" value="{{$event->id}}">
                                <input type="hidden" name="ticket_category_type" value="{{$categorie->type}}">
                                <div class="input-wrapper">
                                    <div class="row">
                                        <div class="col">
                                            <label class="form-label">Name</label>
                                            <input type="text" class="form-control" name="ticket_category"
                                                placeholder="Name" value="{{$categorie->ticket_category}}">
                                        </div>
                                        <div class="col">
                                            <label class="form-label">Price</label>
                                            <input type="number" class="form-control" name="price" placeholder="Price"
                                                value="{{$categorie->price}}">
                                        </div>
                                        <div class="col">
                                            <label class="form-label" for="fee">Fee</label>
                                            <input type="number" class="form-control" name="fee" placeholder="Fee"
                                                value="{{$categorie->fee}}">
                                        </div>
                                        <div class="col">
                                            <label class="form-label">Retail</label>
                                            <input type="text" class="form-control input-reset"
                                                name="calculated_ticket_retail_price"
                                                value="{{$calculated_ticket_retail_price}}" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="input-wrapper">
                                    <div class="row">
                                        <div class="col">
                                            <label class="form-label">Category Description</label>
                                            <input type="text" class="form-control" name="category_description"
                                                placeholder="Description" value="{{$categorie->category_description}}">
                                        </div>
                                        <div class="col">
                                            <label class="form-label">Minimum tickets</label>
                                            <input type="number" class="form-control" name="minimum_ticket"
                                                placeholder="Minimum Ticket" value="{{$categorie->minimum_ticket}}">
                                        </div>
                                        <div class="col">
                                            <label class="form-label">Make Active</label>
                                            <select class="form-control" name="active">
                                                <option value="yes" selected>Yes</option>
                                                <option value="no">No</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="input-wrapper">
                                    <label for="start_date" class="form-label">Event Date & Time</label>
                                    <div class="input-group mb-6">
                                        <span class="input-group-text" id="basic-addon2"><i
                                                class="icon-calendar"></i></span>
                                        @php
                                        $category_date =
                                        Carbon\Carbon::parse($categorie->ticket_category_event_date)->format('Y-m-d');
                                        @endphp
                                        <input type="text" class="form-control datepicker"
                                            name="ticket_category_event_date" placeholder="Start Date"
                                            value="{{ $category_date }}">
                                        <span class="input-group-text" id="basic-addon2"><i
                                                class="icon-time"></i></span>
                                        @php
                                        $category_time =
                                        Carbon\Carbon::parse($categorie->ticket_category_event_date)->format('H:i:s');
                                        @endphp
                                        <select class="form-select ticket-time" name="ticket_category_event_time">
                                            <option value="{{$category_time}}" selected>{{$category_time}}
                                            <option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-btn-wrapper">
                                    <a href="#" class="btn btn-danger cat-rem-ticket"><i class="icon-trash"></i>
                                        Remove</a>
                                    <div>
                                        <a href="#" class="btn btn-warning cat-cancel-ticket"><i
                                                class="fa-solid fa-xmark"></i> Cancel</a>
                                        <button type="submit" class="btn btn-success cat-save-ticket"><i
                                                class="icon-save"></i>
                                            Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tCategory-info">
                            <ul>
                                <li>Category Name: <span class="tCategory-name">{{$categorie->ticket_category}}</span>
                                </li>
                                <li class="category-description">{{$categorie->category_description}}</li>
                                <li class="b-bottom">Minimum Ticket: <span
                                        class="minimum-ticket">{{$categorie->minimum_ticket}}</span></li>
                                <li>Price: <span class="list-price">{{$categorie->price}}</span></li>
                                <li>Fee: <span class="list-fee">{{$categorie->fee}}</span></li>
                                <li class="b-bottom">Retail: <span
                                        class="list-retail">{{$calculated_ticket_retail_price}}</span></li>
                                <li>Event Date: <span class="ticket-category-event-date">{{$category_date}}</span></li>
                                <li>Event Time: <span class="ticket-category-event-time">{{$category_time}}</span></li>
                            </ul>
                            <a href="#" class="btn btn-success cat-edit-ticket"><i class="icon-edit"></i></i>
                                Edit</a>
                        </div>
                        <div class="ticket-form">
                            <div class="ticket-table" data-eventID="{{$event->id}}"
                                data-max="{{$event->max_participants}}" data-categoryID="{{$categorie->id}}">
                                <table class="table mb-0">
                                    <thead>
                                        <tr>
                                            <th class="border-gray-200" scope="col">SOLD</th>
                                            <th class="border-gray-200" scope="col">MAX</th>
                                            <th class="border-gray-200" scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (is_array($categorie->eventTicket) || is_object($categorie->eventTicket))
                                        @foreach ($categorie->eventTicket as $key => $ticket)
                                        <tr>
                                            <td class="align-middle"></td>
                                            <td class="align-middle">
                                                <div class="input-wrapper">
                                                    <label></label>
                                                    <input type="number" class="form-control" name="max"
                                                        placeholder="max" data-id="{{$ticket->id}}"
                                                        value="{{$ticket->max}}">
                                                </div>
                                            </td>
                                            <td class="align-middle">
                                                <a class="btn btn-danger btn-maxTicketTrash" style="display: none;"><i
                                                        class="icon-trash"></i></a>
                                                <a class="btn btn-success btn-maxTicketSave" style="display: none;"><i
                                                        class="icon-save"></i></a>
                                                <a class="btn btn-warning btn-maxTicketEdit"><i
                                                        class="icon-edit"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                                <a class="btn btn-success btn-addMaxTicket"
                                    style="float:right; margin: 25px 25px 25px 0px;"> <i class="fa fa-plus"></i> Add
                                    Ticket</a>
                            </div>
                            <div class="maxticket-info">
                                <div class="input-wrapper">
                                    <label>Ticket Information</label>
                                    <textarea name="" id="" cols="15" rows="4" class="form-control"></textarea>
                                    <div class="btn-info-text">
                                        <a class="btn btn-danger btn-infoTrash">
                                            <i class="icon-trash"></i></a>
                                        <a class="btn btn-success btn-infoSave">
                                            <i class="icon-save"></i></a>
                                        <a class="btn btn-warning btn-infoEdit">
                                            <i class="icon-edit"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="loader-function">
                        <i class="fa-solid fa-spinner"></i>
                    </div>
                </div>
            </div>
            @endforeach
            @else
            @endif
        </div>
    </div>
    <div class="button-wrapper">
        <div class="button-holder">
            @php
            if($categories->count() > 0){
            $buttonStyle1 = "display:none";
            $buttonStyle2 = "";
            }else {
            $buttonStyle1 = "";
            $buttonStyle2 = "display:none";
            }
            @endphp
            <div class="full-width btnfirst" style="{{$buttonStyle1}}">
                <h3>Ticket Categories</h3>
                <p>Each ticket category includes information on price, sales tax, title and description.</p>
                <a class='btn btn-success btn-addCategory' data-eventid="{{$event->id}}"
                    data-eventmax="{{$event->max_participants}}" data-eventtype="regular"> <i class="fa fa-plus"></i>
                    Add Ticket Category</a>
            </div>
            <div class="full-width btnsec" style="{{$buttonStyle2}}">
                <a class='btn btn-success btn-addCategory' data-eventid="{{$event->id}}"
                    data-eventmax="{{$event->max_participants}}" data-eventtype="regular"> <i class="fa fa-plus"></i>
                    Add Ticket Category</a>
                <a class='btn btn-success btn-addCategory' data-eventid="{{$event->id}}" data-eventmax="{{$event->id}}"
                    data-eventtype="sponsor"> <i class="fa fa-plus"></i> Add Sponsor Ticket</a>
            </div>
            <div class="btn-text">
                <p>Ticket category is a type of ticket, such as "standing room" or "Early bird". Each ticket
                    category must have at least one ticket circulation to be on sale.</p>
                <p>
                    Each ticket circulation may have individual limits on sales period and number. You can for
                    example create a circulation of 100 tickets that are only sold in a given period.
                </p>
            </div>
            <div class="btn-text">
                <div class="input-wrapper">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" name="override_max_tic_cat"
                            id="override_max_tic_cat">
                        <label class="form-check-label" for="override_max_tic_cat">
                            These categories can "overlap" in sales. When TOTAL is reached, stop sale!
                        </label>
                    </div>
                </div>
                <div class="input-wrapper">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="max_ticket_per_purchase" class="form-label">Max Tickets per purchase</label>
                            <div class="input-group mb-6">
                                <input type="number" class="form-control" id="max_ticket_per_purchase"
                                    name="max_ticket_per_purchase" placeholder="Street Address" value="1">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="max_participants" class="form-label">Maximum Participants</label>
                            <div class="input-group mb-6">
                                <input type="number" class="form-control" id="max_participants" name="max_participants"
                                    placeholder="City" value="500">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="btn-text">
                <a href="" class="btn btn-primary"> <i class="fa-solid fa-arrow-left"></i> Back</a>
            </div>
            <div class="btn-text">
                <a href="" class="btn btn-primary" style="float:right">Next <i class="fa-solid fa-arrow-right"></i></a>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
