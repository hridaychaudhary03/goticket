@extends('admin.main')
@section('main-section')
<div class="inner-heading">
    <h1 class="page-header-title"><i class="icon-flag-alt"></i>General Settings</h1>
</div>
<div class="breadcrumb-wrapper">
    <ul class="breadcrumb-lists">
        <li class="bread-item"><a href="{{url('admin/dashboard')}}">Dashboard</a> <i class="icon-angle-right"></i></li>
        <li class="bread-item active"><a href="#">General Settings</a></li>
    </ul>
</div>
<div class="inner-wrapper">
    @if(session()->has('message'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{session()->get('message')}}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    <div class="card">
        <form class="row g-3 needs-validation" action="{{url('admin/settings/general')}}" method="post">
            @csrf
            <input type="hidden" name="user_id" value="{{Auth::user()->id }}">
            <div class="col-md-4">
                <label for="siteName" class="form-label">Website Name</label>
                <input type="text" class="form-control" id="siteName" name="site_name" placeholder="Website Name"
                    value="{{$General->site_name}}" required>
                <div class="valid-feedback">
                    Looks good!
                </div>
            </div>
            <div class="col-md-4">
                <label for="ContactEmail" class="form-label">Contact Email</label>
                <div class="input-group has-validation">
                    <span class="input-group-text" id="ContactEmailPrepend">@</span>
                    <input type="email" class="form-control" id="ContactEmail" name="contact_email"
                        aria-describedby="ContactEmailPrepend" placeholder="Contact Email"
                        value="{{$General->contact_email}}" required>
                    <div class="invalid-feedback">
                        Please choose a username.
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <label for="contactPhone" class="form-label">Contact Phone</label>
                <div class="input-group has-validation">
                    <span class="input-group-text" id="contactPhonePrepend"><i
                            class="icon-phone icon-flip-horizontal"></i></span>
                    <input type="text" class="form-control" id="contactPhone" name="contact_phone"
                        aria-describedby="contactPhonePrepend" placeholder="Contact Phone"
                        value="{{$General->contact_phone}}" required>
                    <div class="invalid-feedback">
                        Please choose a username.
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <label for="contactAddress" class="form-label">Contact Address</label>
                <div class="input-group has-validation">
                    <input type="text" class="form-control" id="contactAddress" name="contact_address"
                        aria-describedby="contactAddressPrepend" placeholder="Contact Address"
                        value="{{$General->contact_address}}" required>
                    <div class="invalid-feedback">
                        Please choose a username.
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <label for="facebookPageURL" class="form-label">Facebook Page URL</label>
                <div class="input-group has-validation">
                    <span class="input-group-text" id="facebookPageURLPrepend"><i class="icon-facebook"></i></span>
                    <input type="text" class="form-control" id="facebookPageURL" name="facebook_page_url"
                        aria-describedby="facebookPageURLPrepend" placeholder="Facebook Page URL"
                        value="{{$General->facebook_page_url}}" required>
                    <div class="invalid-feedback">
                        Please choose a username.
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <label for="twitterPageURL" class="form-label">Twitter Page URL</label>
                <div class="input-group has-validation">
                    <span class="input-group-text" id="twitterPageURLPrepend"><i class="icon-twitter"></i></span>
                    <input type="text" class="form-control" id="twitterPageURL" name="twitter_page_url"
                        aria-describedby="twitterPageURLPrepend" placeholder="Twitter Page URL"
                        value="{{$General->twitter_page_url}}" required>
                    <div class="invalid-feedback">
                        Please choose a username.
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <label for="youtubePageURL" class="form-label">Youtube Page URL</label>
                <div class="input-group has-validation">
                    <span class="input-group-text" id="youtubePageURLPrepend"><i class="icon-youtube"></i></span>
                    <input type="text" class="form-control" id="youtubePageURL" name="youtube_page_url"
                        aria-describedby="youtubePageURLPrepend" placeholder="Youtube Page URL"
                        value="{{$General->youtube_page_url}}" required>
                    <div class="invalid-feedback">
                        Please choose a username.
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <label for="defaultPageTitle" class="form-label">Default Page Title</label>
                <div class="input-group has-validation">
                    <input type="text" class="form-control" id="defaultPageTitle" name="default_page_title"
                        placeholder="Default Page Title" value="{{$General->default_page_title}}" required>
                    <div class="invalid-feedback">
                        Please choose a username.
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <label for="">Default Language </label>
                <div class="control">
                    <label class="form-check-label" for="ENG">
                        <input class="form-check-input" type="radio" name="default_lang" id="ENG" value="ENG"
                            {{$General->default_lang == 'ENG' ? "checked" : ""}}>
                        English
                    </label>
                    <label class="form-check-label" for="NOR">
                        <input class="form-check-input" type="radio" name="default_lang" id="NOR" value="NOR"
                            {{$General->default_lang == 'NOR' ? "checked" : ""}}>
                        Norwegian
                    </label>
                </div>
            </div>
            <div class="col-md-6">
                <label for="defaultPageTitle" class="form-label">Default Meta Key</label>
                <div class="input-group has-validation">
                    <textarea name="default_meta_keys" class="form-control" id="" cols="30"
                        rows="3">{{$General->default_meta_keys}}</textarea>
                    <div class="invalid-feedback">
                        Please choose a username.
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <label for="defaultPageTitle" class="form-label">Default Meta Description</label>
                <div class="input-group has-validation">
                    <textarea name="default_meta_desc" class="form-control" id="" cols="30"
                        rows="3">{{$General->default_meta_desc}}</textarea>
                    <div class="invalid-feedback">
                        Please choose a username.
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <label for="defaultPageTitle" class="form-label">Site Offline Message</label>
                <div class="input-group has-validation">
                    <textarea name="site_offline_msg" class="form-control" id="" cols="30"
                        rows="3">{{$General->site_offline_msg}}</textarea>
                    <div class="invalid-feedback">
                        Please choose a username.
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <label for="">Website Status</label>
                <div class="control">
                    <label class="form-check-label" for="online">
                        <input class="form-check-input" type="radio" name="site_status" id="online" value="online"
                            {{$General->site_status == 'online' ? "checked" : ""}}>
                        Online
                    </label>
                    <label class="form-check-label" for="offline">
                        <input class="form-check-input" type="radio" name="site_status" id="offline" value="offline"
                            {{$General->site_status == 'offline' ? "checked" : ""}}>
                        Offline
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <label for="eventDisplay" class="form-label">Initial Events Display Count</label>
                <div class="input-group has-validation">
                    <input type="number" class="form-control" id="eventDisplay" name="initial_event_display_count"
                        aria-describedby="youtubePageURLPrepend" placeholder="24"
                        value="{{$General->initial_event_display_count}}" required>
                    <div class="invalid-feedback">
                        Please choose a username.
                    </div>
                </div>
            </div>
            <div class="col-12">
                <button class="btn btn-primary" type="submit">Update <i class="icon-ok"></i></button>
            </div>
        </form>
    </div>
</div>
@endsection
