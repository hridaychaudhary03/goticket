const times = [
    "00:00:00",
    "00:30:00",
    "01:00:00",
    "01:30:00",
    "02:00:00",
    "02:30:00",
    "03:00:00",
    "03:30:00",
    "04:00:00",
    "04:30:00",
    "05:00:00",
    "05:30:00",
    "06:00:00",
    "06:30:00",
    "07:00:00",
    "07:30:00",
    "08:00:00",
    "08:30:00",
    "09:00:00",
    "09:30:00",
    "10:00:00",
    "10:30:00",
    "11:00:00",
    "11:30:00",
    "12:00:00",
    "12:30:00",
    "13:00:00",
    "13:30:00",
    "14:00:00",
    "14:30:00",
    "15:00:00",
    "15:30:00",
    "16:00:00",
    "16:30:00",
    "17:00:00",
    "17:30:00",
    "18:00:00",
    "18:30:00",
    "19:00:00",
    "19:30:00",
    "20:00:00",
    "20:30:00",
    "21:00:00",
    "21:30:00",
    "22:00:00",
    "22:30:00",
    "23:00:00",
    "23:30:00",
];
jQuery(document).ready(function ($) {
    let data_in_time = $('#in_time').attr('data-selected');
    let data_out_time = $('#out_time').attr('data-selected');
    let data_event_publish_time = $('#event_publish_time').attr('data-selected');
    $.each(times, function (key, value) {
        if (value == data_in_time) {
            let option = `<option value="${value}" selected>${value}</option>`;
            $("#in_time").append(option);
        } else {
            let option = `<option value="${value}">${value}</option>`;
            $("#in_time").append(option);
        }
    });
    $.each(times, function (key, value) {
        if (value == data_out_time) {
            let option = `<option value="${value}" selected>${value}</option>`;
            $("#out_time").append(option);
        } else {
            let option = `<option value="${value}">${value}</option>`;
            $("#out_time").append(option);
        }
    });
    $.each(times, function (key, value) {
        if (value == data_event_publish_time) {
            let option = `<option value="${value}" selected>${value}</option>`;
            $("#event_publish_time").append(option);
        } else {
            let option = `<option value="${value}">${value}</option>`;
            $("#event_publish_time").append(option);
        }
    });
    $('.admin-navigation > li.has-dropdown').click(function (event) {
        if ($(this).hasClass("open")) {
            $(this).removeClass('open');
        } else {
            $('.admin-navigation li.nav-item').removeClass('open');
            $(this).addClass('open');
        }
    });
    $('.text-end.avtar-section').click(function () {
        $(this).toggleClass('show-avtar');
    });
    $('.confirm-alert').click(function (e) {
        let messase = $(this).attr('data-message');
        let permition = confirm(messase);
        if (permition == false) {
            e.preventDefault();
        }
        console.log(permition);
    });
    $('.payment-inputs input[name="payment_type"]').click(function () {
        let thisval = $(this).val();
        if (thisval == 1) {
            $(".ehf_invoice_inpu").show();
        } else {
            $(".ehf_invoice_inpu").hide();
            $('.ehf_invoice_inpu input').prop('checked', false);
        }
    });
    // datepicker
    $("#start_date").datepicker(
        { dateFormat: 'yy-mm-dd' }
    );
    $("#end_date").datepicker(
        { dateFormat: 'yy-mm-dd' }
    );
    $("#eventPublishDate").datepicker(
        { dateFormat: 'yy-mm-dd' }
    );
    // bootstrap js
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl)
    });
});

