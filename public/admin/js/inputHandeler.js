jQuery(document).ready(function ($) {
    // Ajax header setup
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'x-csrf-token': $('meta[name="csrf-token"]').attr("content")
            }
        })
    });
    // create event category
    $(document).on("click", '.btn-addCategory', function (event) {
        let eventid = $(this).attr("data-eventid");
        let eventtype = $(this).attr("data-eventtype");
        let eventmax = $(this).attr("data-eventmax");
        let CategoryInput = `<div class="tCategory-item">
        <div class="tCategory-item-wrapper">
            <div class="tCategory">
                <div class="category-form">
                    <form action="#">
                    <input type="hidden" name="id" value="">
                    <input type="hidden" name="event_id" value="${eventid}">
                    <input type="hidden" name="ticket_category_type" value="${eventtype}">
                        <div class="input-wrapper">
                            <div class="row">
                                <div class="col">
                                    <label class="form-label">Name</label>
                                    <input type="text" class="form-control" name="ticket_category"
                                        placeholder="Name" value="">
                                </div>
                                <div class="col">
                                    <label class="form-label">Price</label>
                                    <input type="number" class="form-control" name="price" placeholder="Price"
                                        value="">
                                </div>
                                <div class="col">
                                    <label class="form-label" for="fee">Fee</label>
                                    <input type="number" class="form-control" name="fee" placeholder="Fee"
                                        value="">
                                </div>
                                <div class="col">
                                    <label class="form-label">Retail</label>
                                    <input type="text" class="form-control input-reset"
                                        name="calculated_ticket_retail_price" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="input-wrapper">
                            <div class="row">
                                <div class="col">
                                    <label class="form-label">Category Description</label>
                                    <input type="text" class="form-control" name="category_description"
                                        placeholder="Description" value="">
                                </div>
                                <div class="col">
                                    <label class="form-label">Minimum tickets</label>
                                    <input type="number" class="form-control" name="minimum_ticket"
                                        placeholder="Minimum Ticket" value="">
                                </div>
                                <div class="col">
                                    <label class="form-label">Make Active</label>
                                    <select class="form-control" name="active">
                                        <option value="yes" selected>Yes</option>
                                        <option value="no">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="input-wrapper">
                            <label for="start_date" class="form-label">Event Date & Time</label>
                            <div class="input-group mb-6">
                                <span class="input-group-text" id="basic-addon2"><i
                                        class="icon-calendar"></i></span>
                                <input type="text" class="form-control datepicker"
                                    name="ticket_category_event_date" placeholder="Start Date" value="">
                                <span class="input-group-text" id="basic-addon2"><i
                                        class="icon-time"></i></span>
                                <select class="form-select ticket-time" name="ticket_category_event_time">
                                    <option selected disable>00:00:00
                                    <option>
                                </select>
                            </div>
                        </div>

                    <div class="form-btn-wrapper">
                        <a href="#" class="btn btn-danger cat-rem-ticket"><i class="icon-trash"></i> Remove</a>
                        <div>
                            <a href="#" class="btn btn-warning cat-cancel-ticket"><i
                                    class="fa-solid fa-xmark"></i> Cancel</a>
                            <button  type="submit" class="btn btn-success cat-save-ticket"><i class="icon-save"></i>
                                Save</button>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="tCategory-info">
                    <ul>
                        <li>Category Name: <span class="tCategory-name">??</span></li>
                        <li class="category-description"></li>
                        <li class="b-bottom">Minimum Ticket: <span class="minimum-ticket">??</span></li>
                        <li>Price: <span class="list-price">??</span></li>
                        <li>Fee: <span class="list-fee">??</span></li>
                        <li class="b-bottom">Retail: <span class="list-retail">??</span></li>
                        <li>Event Date: <span class="ticket-category-event-date">??</span></li>
                        <li>Event Time: <span class="ticket-category-event-time">??</span></li>
                    </ul>
                    <a href="#" class="btn btn-success cat-edit-ticket"><i class="icon-edit"></i></i>
                        Edit</a>
                </div>
                <div class="ticket-form">
                    <div class="ticket-table" data-eventID="${eventid}" data-max="${eventmax}">
                        <table class="table mb-0">
                            <thead>
                                <tr>
                                    <th class="border-gray-200" scope="col">SOLD</th>
                                    <th class="border-gray-200" scope="col">MAX</th>
                                    <th class="border-gray-200" scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <a class="btn btn-success btn-addMaxTicket"
                            style="float:right; margin: 25px 25px 25px 0px;"> <i class="fa fa-plus"></i> Add
                            Ticket</a>
                    </div>
                    <div class="maxticket-info">
                        <div class="input-wrapper">
                            <label>Ticket Information</label>
                            <textarea name="" id="" cols="15" rows="4" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="loader-function">
                <i class="fa-solid fa-spinner"></i>
            </div>
        </div>
    </div>`;
        $(".tCategory-wrapper .card").append(CategoryInput);
        button_handeller();
    });
    // Focus event category date
    $(document).on("focus", '.tCategory-item .datepicker', function (event) {
        $(this).datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
    // Focus event category time
    $(document).on("focus", '.tCategory-item .ticket-time', function (event) {
        const newThis = $(this);
        newThis.find('option').remove();
        $.each(times, function (key, value) {
            let newoption = `<option value="${value}">${value}</option>`;
            newThis.append(newoption);
        });
    });
    // Save Event category
    $(document).on("click", '.cat-save-ticket', function (event) {
        event.preventDefault();
        let newthis = $(this);
        let formDatat = newthis.closest('form').serialize();
        $.ajax({
            url: "/admin/event/ticket-category/updateCreate",
            data: formDatat,
            type: "POST",
            beforeSend: function (xhr) {
                newthis.closest('.tCategory-item').addClass("processing");
            },
            success: function (data) {
                newthis.closest('.tCategory-item').removeClass("processing");
                newthis.closest('.tCategory').addClass('ticket-section');
                newthis.closest('form').find('input[name="id"]').val(data);
            }
        })
    });
    // Remove event category
    $(document).on("click", '.cat-rem-ticket', function (event) {
        let newthis = $(this);
        let catId = newthis.closest('form').find('input[name="id"]').val();
        confirm(`Are you sure to remove this category !`);
        if (catId != "") {
            $.ajax({
                url: `/admin/event/ticket-category/delete/${catId}`,
                type: "DELETE",
                beforeSend: function (xhr) {
                    newthis.closest('.tCategory-item').addClass("processing");
                },
                success: function (data) {
                    newthis.closest('.tCategory-item').removeClass("processing");
                    newthis.closest('.tCategory-item').remove();
                    button_handeller();
                }
            });
        } else {
            setTimeout(function () {
                newthis.closest('.tCategory-item').remove();
                button_handeller();
            }, 500);
        }
    });
    // cancel event category
    $(document).on("click", '.cat-cancel-ticket', function (event) {
        let newthis = $(this);
        let catId = newthis.closest('form').find('input[name="id"]').val();
        if (catId != "") {
            newthis.closest('.tCategory').addClass('ticket-section');
        } else {
            newthis.closest('.tCategory-item').remove();
            button_handeller();
        }

    });
    // create max ticket
    $(document).on("click", '.btn-addMaxTicket', function (event) {
        let newthis = $(this);
        let catId = newthis.closest('form').find('input[name="id"]').val();
        newthis.closest('.ticket-table').attr("data-categoryid", catId);
        let addMaxTicket = `<tr>
        <td class="align-middle"></td>
        <td class="align-middle">
            <div class="input-wrapper">
                <label></label>
                <input type="number" class="form-control" name="max"
                    placeholder="max" data-id="" value="">
            </div>
        </td>
        <td class="align-middle">
            <a class="btn btn-danger btn-maxTicketTrash"><i class="icon-trash"></i></a>
            <a class="btn btn-success btn-maxTicketSave"><i class="icon-save"></i></a>
            <a class="btn btn-warning btn-maxTicketEdit" style="display: none;"><i class="icon-edit"></i></a>
        </td>
    </tr>`;
        newthis.closest('.ticket-table').find('tbody').append(addMaxTicket);
    });
    // save max event ticket
    $(document).on("click", '.ticket-table .btn-maxTicketSave', function (event) {
        let newthis = $(this);
        let data_eventid = newthis.closest('.tCategory-item').find("input[name='event_id']").val();
        console.log(data_eventid);
        let data_max = newthis.closest('.tCategory-item').find("input[name='minimum_ticket']").val();
        let data_categoryid = newthis.closest('.tCategory-item').find("input[name='id']").val();
        let mainID = newthis.closest('tr').find("input[name='max']").attr('data-id');
        let newVal = newthis.closest('tr').find("input[name='max']").val();
        let newData = {
            'id': mainID,
            'categoryid': data_categoryid,
            'max': newVal,
            'eventid': data_eventid,
        };
        if (Number(newVal) <= Number(data_max)) {
            $.ajax({
                url: "/admin/event/event-ticket/updateCreate",
                type: "POST",
                data: newData,
                beforeSend: function (xhr) {
                    newthis.closest('.tCategory-item').addClass("processing");
                },
                success: function (data) {
                    newthis.closest('.tCategory-item').removeClass("processing");
                    newthis.closest('.tCategory').addClass('ticket-section');
                    newthis.closest('tr').find("input[name='max']").attr('data-id', data);
                    newthis.closest('tr').find("input[name='max']").attr('disabled', 'disabled');
                    newthis.closest('tr').find(".btn-maxTicketTrash").hide();
                    newthis.closest('tr').find(".btn-maxTicketSave").hide();
                    newthis.closest('tr').find(".btn-maxTicketEdit").show();
                    $("#flash-success-message").text(`Ticket max value successfully added`);
                    $("#flash-success-message").fadeIn().delay(1000).fadeOut();
                }
            });
        } else {
            $("#flash-error-message").text(`maximum ticket val is ${data_max}`);
            $("#flash-error-message").fadeIn().delay(1000).fadeOut();
        }
    });
    // remove max event ticket
    $(document).on("click", '.ticket-table .btn-maxTicketTrash', function (event) {
        let newthis = $(this);
        let ticketId = newthis.closest('tr').find('input[name="max"]').attr('data-id');
        confirm(`Are you sure to remove this ticket !`);
        if (ticketId != "") {
            $.ajax({
                url: `/admin/event/event-ticket/delete/${ticketId}`,
                type: "DELETE",
                beforeSend: function (xhr) {
                    newthis.closest('.tCategory-item').addClass("processing");
                },
                success: function (data) {
                    newthis.closest('.tCategory-item').removeClass("processing");
                    newthis.closest('tr').remove();
                    $("#flash-success-message").text(`Ticket successfully removed`);
                    $("#flash-success-message").fadeIn().delay(1000).fadeOut();
                    button_handeller();
                }
            });
        } else {
            setTimeout(function () {
                newthis.closest('tr').remove();
                button_handeller();
            }, 500);
        }

        // let newthis = $(this);
        // newthis.closest('.tCategory-item').addClass("processing");
        // setTimeout(function () {
        //     newthis.closest('.tCategory-item').removeClass("processing");
        //     newthis.closest('tr').remove();
        // }, 500);
    });
    // edite max event ticket
    $(document).on("click", '.cat-edit-ticket', function (event) {
        $(this).closest('.tCategory').removeClass('ticket-section');
    });

    $(document).on("click", '.btn-maxTicketEdit', function (event) {
        let newthis = $(this);
        newthis.closest('tr').find(".btn-maxTicketTrash").show();
        newthis.closest('tr').find(".btn-maxTicketSave").show();
        newthis.hide();
        newthis.closest('tr').find("input[name='max']").removeAttr("disabled");
    });

    $(document).on("input", '.category-form input[name="ticket_category"]', function (event) {
        let text = $(this).val();
        $(this).closest(".tCategory-item").find('.tCategory-name').text(text);
    });
    $(document).on("input", '.category-form input[name="price"]', function (event) {
        let text = $(this).val();
        let newVal = Number(text) + 15;
        $(this).closest(".tCategory-item").find('.list-price').text(text);
        $(this).closest(".tCategory-item").find('.list-fee').text(15);
        $(this).closest(".tCategory-item").find('.list-retail').text(newVal);
        $(this).closest(".tCategory-item").find('input[name="fee"]').val(15);
        $(this).closest(".tCategory-item").find('input[name="calculated_ticket_retail_price"]').val(newVal);
    });
    $(document).on("input", '.category-form input[name="fee"]', function (event) {
        let text = $(this).closest(".tCategory-item").find('input[name="price"]').val();
        let fee = $(this).val();
        let newVal = Number(text) + Number(fee);
        $(this).closest(".tCategory-item").find('.list-price').text(text);
        $(this).closest(".tCategory-item").find('.list-fee').text(fee);
        $(this).closest(".tCategory-item").find('.list-retail').text(newVal);
        $(this).closest(".tCategory-item").find('input[name="calculated_ticket_retail_price"]').val(newVal);
    });
    $(document).on("input", '.category-form input[name="category_description"]', function (event) {
        let text = $(this).val();
        $(this).closest(".tCategory-item").find('.category-description').text(text);
    });
    $(document).on("input", '.category-form input[name="minimum_ticket"]', function (event) {
        let text = $(this).val();
        $(this).closest(".tCategory-item").find('.minimum-ticket').text(text);
    });
    $(document).on("change", '.category-form input[name="ticket_category_event_date"]', function (event) {
        let text = $(this).val();
        $(this).closest(".tCategory-item").find('.ticket-category-event-date').text(text);
    });
    $(document).on("change", '.category-form select[name="ticket_category_event_time"]', function (event) {
        let text = $(this).val();
        $(this).closest(".tCategory-item").find('.ticket-category-event-time').text(text);
    });

    $(document).on("click", "#main-advs .adsController.card", function () {
        let newVal = $(this);
        let data_src = $("#main-ads-img").attr("data-src");
        let boxRepeatArea = newVal.find("input[name='advs']").val();
        $("#main-advs .adsController").removeClass("active");
        newVal.addClass("active");
        // alert(boxRepeatArea);
        $("#main-ads-img .adsController").remove();
        for (let i = 1; i <= Number(boxRepeatArea); i++) {
            let newAidsBlocks = `<div class="adsController card ">
        <label for="advsImg${i}">
            <img src="${data_src}" alt="Advertiesment Image">
        </label>
        <input type="file" id="advsImg${i}" name="aidsImage[]" required>
    </div>`;
            $("#main-ads-img").append(newAidsBlocks);
        }
    });

    $(document).on("change", "#main-ads-img .adsController input[type='file']", function () {
        let newThis = $(this);
        let file = newThis.get(0).files[0];
        // var file = $("#uploadImage").get(0).files[0];
        if (file) {
            var reader = new FileReader();
            reader.onload = function () {
                newThis.closest(".adsController").find("img").attr("src", reader.result);
            }
            reader.readAsDataURL(file);
        }
    });

    $(document).on("change", "#main-ads-img .adsController label", function () {
        let newThis = $(this).closest(".adsController").find("input[type='file']");
        let file = newThis.get(0).files[0];
        // var file = $("#uploadImage").get(0).files[0];
        if (file) {
            var reader = new FileReader();
            reader.onload = function () {
                newThis.closest(".adsController").find("img").attr("src", reader.result);
            }
            reader.readAsDataURL(file);
        }
    });

    // Event category handeller
    function button_handeller() {
        var myElements = $(".tCategory-item");
        if (myElements.length) {
            $('.button-holder .full-width.btnsec').show();
            $('.button-holder .full-width.btnfirst').hide();
        } else {
            $('.button-holder .full-width.btnsec').hide();
            $('.button-holder .full-width.btnfirst').show();
        }
    }
});
