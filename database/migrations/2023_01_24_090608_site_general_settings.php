<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generalSettings', function (Blueprint $table) {
            $table->id();
            $table->string('site_name');
            $table->string('user_id');
            $table->string('contact_email');
            $table->string('contact_phone');
            $table->string('contact_address');
            $table->string('facebook_page_url');
            $table->string('twitter_page_url');
            $table->string('youtube_page_url');
            $table->text('default_page_title');
            $table->enum('default_lang',['ENG','NOR']);
            $table->text('default_meta_keys');
            $table->text('default_meta_desc');
            $table->text('site_offline_msg');
            $table->enum('site_status',['online','offline']);
            $table->string('initial_event_display_count');
            $table->rememberToken();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('generalSettings');
    }
};
