<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('slug');
            $table->string('user_id')->nullable();
            $table->longtext('description')->nullable();
            $table->string('start_date');
            $table->string('end_date');
            $table->string('event_publish_date');
            $table->string('max_participants');
            $table->enum('status',['published','unpublished','draft'])->nullable()->default('unpublished');
            $table->enum('Requested_status',['processing','active','cancelled'])->nullable()->default('processing');
            $table->string('street_address')->nullable();
            $table->string('city')->nullable();
            $table->string('event_form_location')->nullable();
            $table->string('event_form_featured')->nullable();
            $table->string('split_price')->nullable();
            $table->string('payment_type')->nullable();
            $table->string('is_ehf_invoice')->nullable();
            $table->string('event_form_vat')->nullable();
            $table->string('invoice_fee')->nullable();
            $table->string('add_instruction')->nullable();
            $table->string('event_form_send_ticket')->nullable();
            $table->string('due_date')->nullable();
            $table->string('invoice_due_date_text_flag')->nullable();
            $table->string('invoice_account_no')->nullable();
            $table->string('no_registration')->nullable();
            $table->string('image')->nullable();
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
};
