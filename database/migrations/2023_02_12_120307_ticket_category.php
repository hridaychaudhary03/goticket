<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_category', function (Blueprint $table) {
            $table->id();
            $table->string('ticket_category');
            $table->integer('event_id');
            $table->longText('category_description');
            $table->integer('minimum_ticket')->nullable();
            $table->decimal('price');
            $table->decimal('fee');
            $table->dateTime('ticket_category_event_date')->nullable();
            $table->string('promo_code')->nullable();
            $table->integer('max')->nullable();
            $table->integer('sold')->default(0);
            $table->integer('reservation')->nullable();
            $table->string('ticket_info')->nullable();
            $table->enum('type', ['regular', 'sponsor'])->default('regular');
            $table->enum('active', ['yes', 'no'])->default('yes');
            $table->tinyInteger('sold_out_email')->default(0);
            $table->tinyInteger('can_overlap')->default(0);
            $table->tinyInteger('can_steal')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_category');
    }
};
