<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\EventsController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\frontController;
use App\Http\Controllers\TicketCategoryController;
use App\Http\Controllers\eventTicketController;
use App\Http\Controllers\advertiesmentController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/', function () {
//     // return view('welcome');
//     return view('front.pages.front');
// });
Route::get('/', [frontController::class, 'frontPage'])->name('frontPage');
Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');


Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'role:admin']], function () {
    Route::get('/dashboard', function () {
        return view('admin.dashboard');
    });
    Route::get('/users', [AdminController::class, 'getAdministrator']);
    Route::get('/time-zone', [AdminController::class, 'getTimezone'])->name('admin.timezone');
    Route::post('/time-zone', [AdminController::class, 'updateTimezone'])->name('admin.updatetimezone');
    Route::get('/settings/general', [AdminController::class, 'getGeneralStings'])->name('admin.getGeneralSetings');
    Route::get('/event/add-event', [AdminController::class, 'addEvents'])->name('admin.addEvents');
    Route::get('/event/event-list', [EventsController::class, 'eventLists'])->name('admin.eventlists');
    Route::post('/event/event-store', [EventsController::class, 'storeEvent'])->name('admin.storeEvent');
    Route::get('/event/event-trash/{id}', [EventsController::class, 'trashEvent'])->name('admin.trashEvent');
    Route::get('/event/event-trashed', [EventsController::class, 'trashedEvent'])->name('admin.trashedEvent');
    Route::get('/event/event-edit/{id}', [EventsController::class, 'editEvent'])->name('admin.editEvent');
    Route::post('/event/event-update/{id}', [EventsController::class, 'updateEvent'])->name('admin.updateEvent');
    Route::get('/event/event-cancel-list', [EventsController::class, 'cancelEventList'])->name('admin.cancelEventList');
    Route::get('/event/event-cancel/{id}', [EventsController::class, 'cancelEvent'])->name('admin.cancelEvent');
    Route::get('/event/event-processing/{id}', [EventsController::class, 'processingEvent'])->name('admin.processingEvent');
    Route::get('/event/event-resore/{id}', [EventsController::class, 'restoreEvent'])->name('admin.restoreEvent');
    Route::get('/event/event-delete/{id}', [EventsController::class, 'deleteEvent'])->name('admin.deleteEvent');
    Route::post('/settings/general', [AdminController::class, 'updateGeneralStings'])->name('admin.updateGeneralSetings');
    Route::get('/event/ticket-category/{id}', [TicketCategoryController::class, 'addTicketCategory'])->name('admin.TicketCategory');
    Route::post('/event/ticket-category/updateCreate', [TicketCategoryController::class, 'updateCreateTicketCategory'])->name('admin.updateCreateTicketCategory');
    Route::delete('/event/ticket-category/delete/{id}', [TicketCategoryController::class, 'deleteTicketCategory'])->name('admin.deleteTicketCategory');
    Route::post('/event/event-ticket/updateCreate', [eventTicketController::class, 'updateCreateEventTicket'])->name('admin.updateCreateEventTicket');
    Route::delete('/event/event-ticket/delete/{id}', [eventTicketController::class, 'deleteEventTicket'])->name('admin.deleteEventTicket');
    Route::get('/event/event-advertiesment/{id}', [advertiesmentController::class, 'getAdvertiesment'])->name('admin.getAdvertiesment');
    Route::post('/event/createOrUpdate-advertiesment', [advertiesmentController::class, 'createOrUpdateAdvertiesment'])->name('admin.createOrUpdateAdvertiesment');
});
Route::middleware(['auth'])->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});
require __DIR__ . '/auth.php';
